#Ab Mapper Guide

The sample ASP.NET Core Application lives in the "AspSchoolApi" project.

In order to run the sample app:

* .NET 5 sdk is required
* (In the AspSchoolApi project) Copy appsettings_default.json to appsettings.json
* Swap the postgres connection string "SchoolDb" for a valid connection string to an existent database
* The db should be empty when starting the sample app, the structure will be built from the configured models
* Run api by running `dotnet run` within the AspSchoolApi project
* The swagger documentation for the API is now available under http://localhost:5000/swagger
  * Sample data is supplied for the post request

Running the unit tests:

* Swap hardcoded connection string in MapperTests.cs Setup and SetupTests.cs const

In order to view the doxygen documentation, open the index.html file under docs/html.

##Features

Not implemented and not completed features:

* Locking
* Inheritance (every class in hierarchy has own table with all columns, including ones defined by base type)
    * Base type queries do not return contents of inheriting classes' tables

Implemented features:

* Saving data
    * Insert object ✔️
    * Update object ✔️
    * Implements and uses change tracking ✔️
    * Caching when saving objects ✔️
* Loading data
    * Loading objects ✔️
    * Caching when loading objects  ✔️
* Queries
    * Custom queries ✔️
    * Direct object access ✔️
    * Intuitive interface (Fluent API) ✔️
* 1:n
    * Loading supported ✔️
    * Saving supported ✔️
* m:n
    * Loading supported ✔️
    * Saving supported ✔️
* Documentation
    * Complete (Comments) ✔️
    * Informative and comprehensible ✔️
* Metadata
    * Implemented ✔️
* Mapping
    * Mapping implemented (code first) ✔️
    * Mapping connects db entries and objects in a comprehensible and configurable manner ✔️
    * Supports a good development experience ✔️
* Features
    * Other features: Transactions implemented ✔️
* Test
    * Test application implemented (ASP.NET Core API) ✔️
    * Test application works as expected ✔️
    * Unit tests implemented (>30) ✔
    * Test are complete and understandable ✔️
* Quality
    * Code is structured well, comprehensible, easy to maintain and does not contain coding errors ✔️
    * API is intuitive, self explaining and easy to use ✔️️
    * Framework is reliable and performs well ✔️
    