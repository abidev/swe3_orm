using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbMapper.Core.Exceptions;
using AbMapper.Core.Interfaces;
using AbMapper.Core.Models;
using AbMapper.Core.Reflection;
using AbMapper.DataAdapters.Postgresql.Models;
using AbMapper.DataAdapters.Postgresql.QueryBuilders;
using Npgsql;
using Serilog;

namespace AbMapper.DataAdapters.Postgresql
{
    /// <summary>
    /// Manages postgresql data source initialization.
    /// </summary>
    public class PostgresqlInitializer
    {
        protected IDataSourceConfig Config { get; }
        
        protected NpgsqlConnection Connection { get; }
        
        /// <summary>
        /// Contains tables found in postgresql metadata tables.
        /// </summary>
        private Dictionary<string, Table> _tables;
        
        /// <summary>
        /// Queue containing discovered relationships to be added to entities after table creations.
        /// </summary>
        private readonly Queue<Entity> _entitiesWithRelations = new Queue<Entity>();
        
        /// <summary>
        /// Sets config and creates a postgresql connection object.
        /// </summary>
        /// <param name="config">Config containing connection string</param>
        public PostgresqlInitializer(IDataSourceConfig config)
        {
            Config = config;
            Connection = new NpgsqlConnection(config.ConnectionString);
        }
        
        /// <summary>
        /// Initialization of the datasource. Called once during AbMapper initialization.
        /// </summary>
        public async Task InitializeAsync()
        {
            if (Connection == null || Connection.State != ConnectionState.Open)
                await OpenConnectionAsync();
        }

        /// <summary>
        /// Called after all entities are in sync with AbMapper metadata. Can be used to setup relationships.
        /// </summary>
        public async Task SetupRelationshipsAsync()
        {
            while (_entitiesWithRelations.TryDequeue(out var entity))
            {
                var alterTableStmt = new AlterTableQuery(entity.TableName);
                // related table -> list ( col, related col )
                var foreignKeys = new Dictionary<string, List<KeyValuePair<string, string>>>();
                var deleteBehaviours = new Dictionary<string, DeleteBehaviour?>();
                
                foreach (var feInfo in entity.ForeignEntityInfos)
                {
                    foreach (var fkInfo in feInfo.ForeignKeyInfos)
                    {
                        var keyValPair =
                            new KeyValuePair<string, string>(fkInfo.LocalProperty.ColumnName, fkInfo.ForeignProperty.ColumnName);
                        
                        if (feInfo.ForeignEntity.TableName == entity.TableName) continue;
                        
                        if (foreignKeys.ContainsKey(feInfo.ForeignEntity.TableName))
                            foreignKeys[feInfo.ForeignEntity.TableName].Add(keyValPair);
                        else
                            foreignKeys.Add(feInfo.ForeignEntity.TableName, 
                                new List<KeyValuePair<string, string>> { keyValPair });
                        
                        if (!deleteBehaviours.ContainsKey(feInfo.ForeignEntity.TableName)) 
                            deleteBehaviours.Add(feInfo.ForeignEntity.TableName, fkInfo.LocalProperty.DeleteBehaviour);
                    }
                }

                foreach (var (key, value) in foreignKeys)
                {
                    alterTableStmt.AddForeignKeyConstraint(
                        value.Select(fk => fk.Key), 
                        key, 
                        value.Select(fk => fk.Value), deleteBehaviours[key]);
                }
                
                if (!alterTableStmt.HasActions) continue;
                
                await using var cmd = new NpgsqlCommand(alterTableStmt.Finish(), Connection);
                await cmd.ExecuteNonQueryAsync();
            }
            Log.Logger.Verbose("{Class}: Added foreign key information to db", nameof(PostgresqlInitializer));
        }

        /// <summary>
        /// Opens a database connection.
        /// </summary>
        private async Task OpenConnectionAsync()
        {
            await Connection.OpenAsync();
            Log.Logger.Verbose("{Class}: Opened db connection", nameof(PostgresqlInitializer));
        }

        /// <summary>
        /// Reads postgresql table and column metadata and saves it to _tables dictionary.
        /// </summary>
        private async Task ReadTableInfoAsync()
        {
            _tables = new Dictionary<string, Table>();
            var tableNames = new List<string>();

            string tablesStmt = "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public';";
            await using (var tablesCmd = new NpgsqlCommand(tablesStmt, Connection))
            {
                await using var tablesReader = await tablesCmd.ExecuteReaderAsync();
                while (await tablesReader.ReadAsync())
                {
                    tableNames.Add(tablesReader.GetString(0));
                }
            }

            foreach (var tableName in tableNames)
            {
                if (Config.WipeDatabaseOnStartup)
                {
                    await using var cmd = new NpgsqlCommand(new DropTableQuery(tableName).Finish(), Connection);
                    await cmd.ExecuteNonQueryAsync();
                    Log.Logger.Verbose("{Class}: Dropped table {Table}", nameof(PostgresqlInitializer), tableName);
                    
                    continue;
                }
                
                var table = new Table(tableName);
                var colsStmt = $"SELECT column_name, data_type FROM information_schema.columns WHERE table_name = '{tableName}';";
                await using var colsCmd = new NpgsqlCommand(colsStmt, Connection);
                await using var colsReader = await colsCmd.ExecuteReaderAsync();
                while (await colsReader.ReadAsync())
                {
                    table.AddColumn(new Column
                    {
                        Name = colsReader.GetString(0)
                    });
                }
                
                _tables.Add(tableName, table);
            }
            Log.Logger.Verbose("{Class}: Read table info from information schema", nameof(PostgresqlInitializer));
        }

        /// <summary>
        /// Syncs an entity against the datasource. (adjusts the structure, ddl)
        /// </summary>
        /// <param name="entity">Entity to sync</param>
        public async Task SyncEntityAsync(Entity entity)
        {
            if (_tables == null) await ReadTableInfoAsync();

            if (_tables.ContainsKey(entity.TableName)) // Table exists
            {
                var alterTableStmt = new AlterTableQuery(entity.TableName);
                var colsToSync = new Dictionary<string, Column>(_tables[entity.TableName].Columns);
                var propsToSync = new List<Property>(entity.Properties.Where(p => !p.IsExcluded));
                foreach (var prop in propsToSync)
                {
                    if (colsToSync.ContainsKey(prop.ColumnName)) // Column exists in table, sync ok
                    {
                        colsToSync.Remove(prop.ColumnName);
                    }
                    else // There is no col for this prop yet, add col
                    {
                        var modifierBuilder = new StringBuilder();
                        if (prop.IsPrimary)
                        {
                            Log.Logger.Warning("{Class}: Primary keys cannot be added without rebuilding the database, ignoring pk", nameof(PostgresqlInitializer));
                        }
                        if (prop.NotNull) modifierBuilder.Append(" NOT NULL");
                        
                        if (prop.IsSerial && prop.Type == typeof(int)) 
                            alterTableStmt.AddColumn(prop.ColumnName, "SERIAL", modifierBuilder.ToString());
                        else if (prop.IsSerial) throw new AbDataSourceException($"Property {prop.Name} is of type {prop.Type}. Type int is required to use serial");
                        else if (prop.IsEnum) alterTableStmt.AddColumn(prop.ColumnName, "INTEGER", modifierBuilder.ToString());
                        else 
                            alterTableStmt.AddColumn(prop.ColumnName,
                                prop.Type == typeof(Type) ? typeof(string) : prop.Type, modifierBuilder.ToString());
                    }
                }
                
                if (Config.DropRemovedColumns && colsToSync.Count > 0)
                {
                    foreach (var col in colsToSync)
                    {
                        alterTableStmt.DropColumn(col.Value.Name);
                    }
                }

                if (alterTableStmt.HasActions)
                {
                    await using var cmd = new NpgsqlCommand(alterTableStmt.Finish(), Connection);
                    await cmd.ExecuteNonQueryAsync();
                }
            }
            else
            {
                await CreateTableFromModelAsync(entity);
            }
            
            Log.Logger.Verbose("{Class}: Synchronized entity {Entity}", nameof(PostgresqlInitializer), entity.Type.Name);
        }
        
        /// <summary>
        /// Creates a table from the given entity metadata.
        /// </summary>
        /// <param name="entity"></param>
        /// <exception cref="AbDataSourceException">Thrown if a serial column has non int type</exception>
        private async Task CreateTableFromModelAsync(Entity entity)
        {
            var createStmt = new CreateTableQuery(entity.TableName, true);
            var entityHasRelationship = entity.HasRelatedEntity;
            var propCols = new List<Property>(entity.Properties);

            foreach (var prop in propCols)
            {
                if (prop.IsExcluded) continue; // Skip excluded columns
                Type propType = prop.Type;
                // Ignore enumerable types (will be relevant for relations)
                if (propType.IsArray || propType != typeof(string) && propType.GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>))) continue;

                if (propType == typeof(Type)) propType = typeof(string);
                
                var modifierBuilder = new StringBuilder();
                if (prop.NotNull) modifierBuilder.Append(" NOT NULL");

                if (prop.IsForeign) entityHasRelationship = true;
                
                if (prop.IsSerial && prop.Type == typeof(int)) createStmt.AddColumn(prop.ColumnName, "SERIAL", modifierBuilder.ToString(), prop.IsPrimary);
                else if (prop.IsSerial) throw new AbDataSourceException($"Property {prop.Name} is of type {prop.Type}. Type int is required to use serial");
                else if (prop.IsEnum) createStmt.AddColumn(prop.ColumnName, "INTEGER", modifierBuilder.ToString(), prop.IsPrimary);
                else createStmt.AddColumn(prop.ColumnName, propType, modifierBuilder.ToString(), prop.IsPrimary);
            }
            
            if (entityHasRelationship) _entitiesWithRelations.Enqueue(entity);

            await using var cmd = new NpgsqlCommand(createStmt.Finish(), Connection);
            await cmd.ExecuteNonQueryAsync();
            
            if (entity.IsMapperManaged)
                Log.Logger.Verbose("{Class}: Checked or created table for mapper managed table {Entity}", nameof(PostgresqlInitializer), entity.TableName);
            else
                Log.Logger.Verbose("{Class}: Checked or created table for entity {Entity}", nameof(PostgresqlInitializer), entity.Type?.Name);
        }
    }
}