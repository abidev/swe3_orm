using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace AbMapper.DataAdapters.Postgresql.Models
{
    /// <summary>
    /// Holds information about a table. (gathered from postgres metadata tables)
    /// </summary>
    public class Table
    {
        public string Name { get; private set; }
        
        private readonly Dictionary<string, Column> _columns = new Dictionary<string, Column>();
        public IDictionary<string, Column> Columns => new ReadOnlyDictionary<string, Column>(_columns);

        public Table(string tableName)
        {
            Name = tableName;
        }
        
        public void AddColumn(Column col)
        {
            _columns.Add(col.Name, col);
        }
    }
}