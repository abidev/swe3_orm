namespace AbMapper.DataAdapters.Postgresql.Models
{
    /// <summary>
    /// Represents a database column.
    /// </summary>
    public class Column
    {
        public string Name { get; set; }
        public bool IsPrimary { get; set; }
    }
}