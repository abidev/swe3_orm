namespace AbMapper.DataAdapters.Postgresql.Models
{
    /// <summary>
    /// Holds information for a related table and column.
    /// </summary>
    public class DbForeignKeyInfo
    {
        public string ForeignTableName { get; set; }
        public string ForeignColumnName { get; set; }
    }
}