using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AbMapper.Core;
using AbMapper.Core.Exceptions;
using AbMapper.Core.Interfaces;
using AbMapper.Core.Models;
using AbMapper.Core.Reflection;
using AbMapper.DataAdapters.Postgresql.QueryBuilders;
using Npgsql;
using Serilog;

namespace AbMapper.DataAdapters.Postgresql
{
    /// <summary>
    /// Implements a postgresql db datasource.
    /// </summary>
    public class PostgresqlDataSource : PostgresqlInitializer, IDataSource, IDisposable
    {
        public PostgresqlDataSource(IDataSourceConfig config): base(config) {}

        #region Get

        /// <summary>
        /// Queries an entry of the given entity.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="foreignEntityInfos"></param>
        /// <param name="whereExpressions"></param>
        /// <param name="pkValues">Primary key values to search for.</param>
        /// <typeparam name="T"></typeparam>
        /// <returns>
        ///    Desired entry object: If found.
        ///    T default (eg. null): If not found.
        /// </returns>
        public async Task<T> GetAsync<T>(Entity entity, IEnumerable<ForeignEntityInfo> foreignEntityInfos = null, 
            IEnumerable<BinaryExpression> whereExpressions = null, params object[] pkValues) where T: class, new()
        {
            var pks = entity.PrimaryKeys;
            if (pks.Count != pkValues.Length)
            {
                Log.Logger.Warning("{Class}: Entity {Entity} has {PkCount} primary keys, got {GotCount} pk values", 
                    nameof(PostgresqlDataSource), entity.Type.Name, entity.PrimaryKeys.Count, pkValues.Length);
                throw new ArgumentException("Number of given pk values does not match number of entity pk values");
            }
            
            var selectStmt = new SelectQuery(entity.Type.Name, "*");
            
            await using var cmd = new NpgsqlCommand { Connection = Connection };
            
            ProcessWhereExpressions(entity, selectStmt, whereExpressions, cmd);
            
            var entityInfos = foreignEntityInfos?.ToList();
            if (entityInfos != null)
            {
                JoinRelatedTables(selectStmt, entity, entityInfos.ToList());
            }

            for (var i = 0; i < pkValues.Length; i++)
            {
                selectStmt.AddConstantColumnConstraint(pks[i].ColumnName, "=");
                cmd.Parameters.AddWithValue(pks[i].ColumnName, pkValues[i]);
            }
            cmd.CommandText = selectStmt.Finish();
            
            await using var reader = await cmd.ExecuteReaderAsync();
            
            var data = await ReadSelectResultRowsAsync<T>(entity, entityInfos, reader);
            
            Log.Logger.Verbose("{Class}: Read entry for entity {Entity}", nameof(PostgresqlDataSource), entity.Type.Name);
            return data.FirstOrDefault();
        }

        /// <summary>
        /// Queries all entries of the given entity.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="eagerLoad"></param>
        /// <param name="foreignEntityInfos">Navigational properties to eager load</param>
        /// <param name="whereExpressions"></param>
        /// <param name="limit"></param>
        /// <param name="skip"></param>
        /// <param name="orderByInfo"></param>
        /// <typeparam name="T"></typeparam>
        public async Task<IEnumerable<T>> GetAllAsync<T>(Entity entity, bool eagerLoad = false,
            IEnumerable<ForeignEntityInfo> foreignEntityInfos = null,
            IEnumerable<BinaryExpression> whereExpressions = null, int? limit = null, int? skip = null,
            OrderByInfo orderByInfo = null) where T: class, new()
        {
            var selectStmt = new SelectQuery(entity.Type.Name, "*");
            selectStmt.SetLimit(limit);
            selectStmt.SetSkipCount(skip);
            if (orderByInfo != null)
            {
                var property = entity.Properties.FirstOrDefault(p => p.Name == orderByInfo.OrderByMember.Name);
                if (property != null)
                {
                    selectStmt.SetOrderByInfo(property.ColumnName, orderByInfo.Direction);
                }
            }
            
            await using var cmd = new NpgsqlCommand {Connection = Connection};
            
            ProcessWhereExpressions(entity, selectStmt, whereExpressions, cmd);
            
            var entityInfos = foreignEntityInfos?.ToList();
            JoinRelatedTables(selectStmt, entity, entityInfos);

            cmd.CommandText = selectStmt.Finish();
            
            await using var reader = await cmd.ExecuteReaderAsync();

            var data = await ReadSelectResultRowsAsync<T>(entity, entityInfos, reader);

            Log.Logger.Verbose("{Class}: Read entries for entity {Entity}", nameof(PostgresqlDataSource), entity.Type.Name);
            return data.AsReadOnly();
        }

        /// <summary>
        /// Adds where statements for each binary expression.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="query"></param>
        /// <param name="expressions"></param>
        /// <param name="cmd"></param>
        private static void ProcessWhereExpressions(Entity entity, SelectQuery query, IEnumerable<BinaryExpression> expressions, NpgsqlCommand cmd)
        {
            foreach (var expression in expressions)
            {
                var col1 = GenerateWhereExpressionColumn(entity, query, expression.Left);
                var col2 = GenerateWhereExpressionColumn(entity, query, expression.Right);
                var operation = GenerateWhereExpressionOperation(expression);

                var prepStmtName = query.AddWhereStatement(col1, operation, col2);

                var constExpr = col1 == null
                    ? expression.Left as ConstantExpression
                    : expression.Right as ConstantExpression;

                if (prepStmtName != null && constExpr != null)
                {
                    cmd.Parameters.AddWithValue(prepStmtName, constExpr.Value);
                }
                else
                {
                    var val = GetMemberValue(expression.Left as MemberExpression) ??
                              GetMemberValue(expression.Right as MemberExpression);
                    if (prepStmtName != null && val != null)
                    {
                        cmd.Parameters.AddWithValue(prepStmtName, val);
                    }
                }
            }
        }
        
        /// <summary>
        /// Compiles a member expression in order to retrieve the members value.
        /// </summary>
        /// <param name="member">Member expression to use.</param>
        /// <returns>Member value.</returns>
        private static object GetMemberValue(MemberExpression member)
        {
            if (member.Expression != null && !(member.Expression is ConstantExpression)) return null;
            
            var objectMember = Expression.Convert(member, typeof(object));

            var getterLambda = Expression.Lambda<Func<object>>(objectMember);

            var getter = getterLambda.Compile();

            return getter();
        }

        /// <summary>
        /// Returns the db operation string for the given binary expression type.
        /// </summary>
        /// <param name="expr"></param>
        private static string GenerateWhereExpressionOperation(BinaryExpression expr)
        {
            switch (expr.NodeType)
            {
                case ExpressionType.Equal:
                    return "=";
                case ExpressionType.NotEqual:
                    return "!=";
                case ExpressionType.GreaterThan:
                    return ">";
                case ExpressionType.GreaterThanOrEqual:
                    return ">=";
                case ExpressionType.LessThan:
                    return "<";
                case ExpressionType.LessThanOrEqual:
                    return "<=";
            }
            
            return null;
        }

        /// <summary>
        /// Generates a string identifying a column of the given entity based on a member expression.
        /// Returns null if expression is not a member expression.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="query"></param>
        /// <param name="expr"></param>
        /// <exception cref="AbInvalidWhereExpressionException">AbMapper is missing information about the selected member</exception>
        private static string GenerateWhereExpressionColumn(Entity entity, SelectQuery query, Expression expr)
        {
            if (expr is UnaryExpression unExpr) expr = unExpr.Operand;
            if (!(expr is MemberExpression memExpr)) return null;
            
            var prop = entity.Properties.FirstOrDefault(p => p.Name == memExpr.Member.Name);
            if (prop == null) return null;

            return $"{query.QueryIdentity}.{prop.ColumnName}";
        }

        /// <summary>
        /// Reads the results of the queried data and returns a list of instances.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="entityInfos"></param>
        /// <param name="reader"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private static async Task<List<T>> ReadSelectResultRowsAsync<T>(Entity entity, List<ForeignEntityInfo> entityInfos, DbDataReader reader) where T: class, new()
        {
            var data = new List<T>();
            while (await reader.ReadAsync())
            {
                var obj = DataRecordToObject<T>(entity, reader, entityInfos);
                var hit = data.FirstOrDefault(o => CompareObjectsByPks(entity, o, obj));
                if (hit != null && entityInfos != null)
                {
                    foreach (var entityInfo in entityInfos)
                    {
                        if (entityInfo.ForeignEntityProperty != null && Util.IsCollectionOfType(entityInfo.ForeignEntityProperty.Type, typeof(RelatedCollection<>)))
                        {
                            var fPropValue = entityInfo.ForeignEntityProperty.PropertyInfo.GetValue(hit);
                            var propValue = entityInfo.ForeignEntityProperty.PropertyInfo.GetValue(obj);
                            if (fPropValue != null && propValue != null)
                            {
                                fPropValue.GetType().GetMethod("AddRange")?.Invoke(fPropValue, new [] { propValue });
                            }
                            else if (propValue != null)
                            {
                                var newCollection = Activator.CreateInstance(entityInfo.ForeignEntityProperty.Type);
                                entityInfo.ForeignEntityProperty.PropertyInfo.SetValue(hit, newCollection);
                                
                                newCollection.GetType().GetMethod("AddRange")?.Invoke(newCollection, new [] { propValue });
                            }
                        }
                    }
                }
                else
                {
                    data.Add(obj);
                }
            }

            return data;
        }

        /// <summary>
        /// Joins related tables to the select query based on foreign entity infos.
        /// </summary>
        /// <param name="selectQuery"></param>
        /// <param name="entity"></param>
        /// <param name="foreignEntityInfos"></param>
        private static void JoinRelatedTables(SelectQuery selectQuery, Entity entity, IReadOnlyCollection<ForeignEntityInfo> foreignEntityInfos)
        {
            if (foreignEntityInfos == null) return;
            
            foreach (var foreignEntityInfo in foreignEntityInfos)
            {
                switch (foreignEntityInfo.NavigationEntity)
                {
                    case null when !foreignEntityInfo.ForeignEntityPropertyIsCollection: // 1:n 1 side
                        selectQuery.AddJoin(foreignEntityInfo.ForeignEntity.TableName,
                            foreignEntityInfo.ForeignKeyInfos.Select(fki => fki.LocalProperty.ColumnName).ToArray(),
                            foreignEntityInfo.ForeignKeyInfos.Select(fki => fki.ForeignProperty.ColumnName).ToArray());
                        break;
                        
                    case null: // 1:n n side
                    {
                        var foreignEntityForeignEntityInfo =
                            foreignEntityInfo.ForeignEntity.ForeignEntityInfos.FirstOrDefault(fei =>
                                fei.ForeignEntity.TableName == entity.TableName);

                        if (foreignEntityForeignEntityInfo == null) break;
                            
                        selectQuery.AddJoin(foreignEntityInfo.ForeignEntity.TableName,
                            foreignEntityForeignEntityInfo.ForeignKeyInfos.Select(fki => fki.ForeignProperty.ColumnName).ToArray(),
                            foreignEntityForeignEntityInfo.ForeignKeyInfos.Select(fki => fki.LocalProperty.ColumnName).ToArray());
                        
                        break;
                    }
                        
                    default: // n:m -> intermediary entity
                        var navEntityForeignEntityInfo =
                            foreignEntityInfo.NavigationEntity.ForeignEntityInfos.FirstOrDefault(fei =>
                                fei.ForeignEntity.TableName == entity.TableName);
                        
                        var navEntityForeignEntityInfoOut =
                            foreignEntityInfo.NavigationEntity.ForeignEntityInfos.FirstOrDefault(fei =>
                                fei.ForeignEntity.TableName == foreignEntityInfo.ForeignEntity.TableName);

                        if (navEntityForeignEntityInfo == null || navEntityForeignEntityInfoOut == null) break;
                        
                        var alias = selectQuery.AddJoin(foreignEntityInfo.NavigationEntity.TableName,
                            navEntityForeignEntityInfo.ForeignKeyInfos.Select(fki => fki.ForeignProperty.ColumnName).ToArray(),
                            navEntityForeignEntityInfo.ForeignKeyInfos.Select(fki => fki.LocalProperty.ColumnName).ToArray());
                        
                        selectQuery.AddJoin(foreignEntityInfo.ForeignEntity.TableName,
                            navEntityForeignEntityInfoOut.ForeignKeyInfos.Select(fki => $"{alias}.{fki.LocalProperty.ColumnName}").ToArray(),
                            navEntityForeignEntityInfoOut.ForeignKeyInfos.Select(fki => fki.ForeignProperty.ColumnName).ToArray(),
                            false);
                        
                        break;
                }
            }
            
            Log.Logger.Verbose("{Class}: Attached related entity data to query for entity {Entity}", nameof(PostgresqlInitializer), entity.Type.Name);
        }

        #endregion
        
        #region Insert

        /// <summary>
        /// Persists entity data to the datasource.
        /// </summary>
        /// <param name="entity">Entity with metadata (including the given values' type)</param>
        /// <param name="v">Object holding data to be persisted</param>
        public async Task InsertAsync(Entity entity, object v)
        {
            var data = Convert.ChangeType(v, entity.Type);
            if (data == null)
            {
                Log.Logger.Warning("{Class}: Given value is not of type {Type}", nameof(PostgresqlDataSource), entity.Type.Name);
                throw new AbDataSourceException($"Given value is not of type {nameof(entity.Type)}");
            }
            
            var insertStmt = new InsertQuery(entity.TableName);
            await using var cmd = new NpgsqlCommand { Connection = Connection };

            var navigationEntityObjects = new Dictionary<Entity, List<object>>();
            
            // Loop over foreign entities
            foreach (var feInfo in entity.ForeignEntityInfos)
            {
                // Look for a value in the local property that belongs to this foreign entity relationship
                var fePropVal = feInfo.ForeignEntityProperty?.PropertyInfo.GetValue(data);
                if (fePropVal == null) continue;

                // Check for a related collection (could be 1:n or n:m)
                if (Util.IsCollectionOfType(fePropVal.GetType(), typeof(RelatedCollection<>)))
                {
                    foreach (var entry in (IEnumerable) fePropVal)
                    {
                        // Insert each related entity into db
                        await InsertAsync(feInfo.ForeignEntity, entry);

                        // In case of n:m relationships: insert into intermediary table
                        if (feInfo.NavigationEntity == null) continue;

                        if (navigationEntityObjects.ContainsKey(feInfo.NavigationEntity))
                            navigationEntityObjects[feInfo.NavigationEntity].Add(entry);
                        else 
                            navigationEntityObjects[feInfo.NavigationEntity] = new List<object> {entry};
                    }
                }
                else
                {
                    await InsertAsync(feInfo.ForeignEntity, fePropVal); // this will cause loops if related entity has ref to this entity
                    
                    foreach (var fkInfo in feInfo.ForeignKeyInfos)
                    {
                        var pkVal = fkInfo.ForeignProperty.PropertyInfo.GetValue(fePropVal);
                        AddPropertyToInsertStatement(fkInfo.LocalProperty, insertStmt, cmd, pkVal);
                    }
                }
            }
            
            Property returningProp = null;
            foreach (var prop in entity.Properties.Where(p => !p.IsExcluded && !p.IsForeign && p.ExistsOnEntityType))
            {
                var res = AddPropertyToInsertStatement(prop, insertStmt, cmd, prop.PropertyInfo.GetValue(data));
                if (res != null) returningProp = res;
            }

            if (entity.Type == typeof(ExpandoObject))
            {
                foreach (var fkProp in entity.ForeignKeys)
                {
                    AddPropertyToInsertStatement(fkProp, insertStmt, cmd, ((IDictionary<string, object>) data)[fkProp.Name]);
                }
            }

            insertStmt.UseConflictBehaviour(InsertQueryConflictBehaviour.Update, entity.PrimaryKeys.Select(pk => pk.ColumnName));
            cmd.CommandText = insertStmt.Finish();
            var result = await cmd.ExecuteScalarAsync();
            if (returningProp != null && result != null)
            {
                returningProp.PropertyInfo.SetValue(data, result);
            }
            
            await InsertIntermediaryTableEntries(navigationEntityObjects, entity, data);
            
            Log.Logger.Verbose("{Class}: Inserted entry into table {Table}", nameof(PostgresqlDataSource), entity.TableName);
        }

        /// <summary>
        /// Inserts entries into intermediary tables for n:m relationships.
        /// </summary>
        /// <param name="navigationEntityObjects"></param>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private async Task InsertIntermediaryTableEntries(Dictionary<Entity, List<object>> navigationEntityObjects, Entity entity, object data)
        {
            foreach (var (navEntity, entries) in navigationEntityObjects)
            {
                foreach (var entry in entries)
                {
                    // Build an object holding the data required for inserting into iterm. table
                    dynamic navigationExpando = new ExpandoObject();
                    var navigationExpandoDict = (IDictionary<string, object>) navigationExpando;

                    foreach (var navFeInfo in navEntity.ForeignEntityInfos)
                    {
                        foreach (var fkInfo in navFeInfo.ForeignKeyInfos)
                        {
                            var navPkVal = fkInfo.ForeignProperty.PropertyInfo.GetValue(navFeInfo.ForeignEntity == entity ? data : entry);
                            // fkInfo.LocalProperty.PropertyInfo.SetValue(navigationObject, navPkVal);
                            if (!navigationExpandoDict.ContainsKey(fkInfo.LocalProperty.Name)) navigationExpandoDict.Add(fkInfo.LocalProperty.Name, navPkVal);
                        }
                    }
                    
                    await InsertAsync(navEntity, navigationExpando);
                }
            }
            
            if (navigationEntityObjects.Count > 0)
                Log.Logger.Verbose("{Class}: Inserted entries into intermediate tables", nameof(PostgresqlDataSource));
        }

        /// <summary>
        /// Adds data of a property to insert statement and adds prepared stmt val.
        /// </summary>
        private static Property AddPropertyToInsertStatement(Property prop, InsertQuery insertStmt, NpgsqlCommand cmd, object data)
        {
            if (prop.IsExcluded) return null; // Skip excluded columns
            if (prop.IsSerial && prop.IsPrimary && (int) data == default)
            {
                insertStmt.SetReturningColumn(prop.ColumnName);
                return prop;
            }
            Type propType = prop.IsEnum ? typeof(int) : prop.Type;
            // Ignore enumerable types (will be relevant for relations)
            if (propType.IsArray || propType != typeof(string) && propType.GetInterfaces()
                .Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>)))
                return null;

            insertStmt.AddData(prop.ColumnName);
            object propVal = data;
            if (prop.IsEnum && propVal != null) propVal = (int) propVal;
            if (propType == typeof(Type)) propVal = propVal?.ToString();
            if (prop.NotNull && propVal == null) throw new AbDataSourceException($"Got null for not null property {prop.Name}");
            propVal ??= DBNull.Value;
            cmd.Parameters.AddWithValue(prop.ColumnName, propVal);

            return null;
        }

        #endregion

        #region Delete

        /// <summary>
        /// Removes entity data from the datasource.
        /// </summary>
        /// <param name="entity">Entity with metadata</param>
        /// <param name="data">Object holding data to be removed</param>
        public async Task RemoveAsync(Entity entity, object data)
        {
            var deleteStmt = new DeleteQuery(entity.TableName);
            await using var cmd = new NpgsqlCommand { Connection = Connection };

            foreach (var pkProperty in entity.PrimaryKeys)
            {
                deleteStmt.AddPkColumn(pkProperty.ColumnName);
                cmd.Parameters.AddWithValue(pkProperty.ColumnName, pkProperty.PropertyInfo.GetValue(data));
            }

            cmd.CommandText = deleteStmt.Finish();

            await cmd.ExecuteNonQueryAsync();
            Log.Logger.Verbose("{Class}: Removed entry from table {Table}", nameof(PostgresqlDataSource), entity.TableName);
        }

        #endregion

        #region Transactions

        /// <summary>
        /// Starts a transaction.
        /// </summary>
        public async Task Begin()
        {
            await using var cmd = new NpgsqlCommand { Connection = Connection, CommandText = "BEGIN;" };
            await cmd.ExecuteNonQueryAsync();
            Log.Logger.Verbose("{Class}: Started db transaction", nameof(PostgresqlDataSource));
        }

        /// <summary>
        /// Commits transaction changes.
        /// </summary>
        public async Task Commit()
        {
            await using var cmd = new NpgsqlCommand { Connection = Connection, CommandText = "COMMIT;" };
            await cmd.ExecuteNonQueryAsync();
            Log.Logger.Verbose("{Class}: Committed db transaction", nameof(PostgresqlDataSource));
        }

        /// <summary>
        /// Rollbacks transaction changes.
        /// </summary>
        public async Task Rollback()
        {
            await using var cmd = new NpgsqlCommand { Connection = Connection, CommandText = "ROLLBACK;" };
            await cmd.ExecuteNonQueryAsync();
            Log.Logger.Verbose("{Class}: Rolled db transaction back", nameof(PostgresqlDataSource));
        }

        #endregion

        #region Helpers
        
        /// <summary>
        /// Retrieves values from datarecord and builds an object with the values.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="record"></param>
        /// <param name="foreignEntityInfos"></param>
        /// <param name="startFieldIndex"></param>
        /// <typeparam name="T">Desired entity type</typeparam>
        /// <returns>Object of entity type</returns>
        private static T DataRecordToObject<T>(Entity entity, IDataRecord record, List<ForeignEntityInfo> foreignEntityInfos = null, int startFieldIndex = 0) where T: class, new()
        {
            return DataRecordToObject(typeof(T), entity, record, foreignEntityInfos, startFieldIndex) as T;
        }

        /// <summary>
        /// Retrieves values from datarecord and builds an object with the values.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="entity"></param>
        /// <param name="record"></param>
        /// <param name="foreignEntityInfos"></param>
        /// <param name="startFieldIndex"></param>
        /// <returns>Object of entity type</returns>
        private static object DataRecordToObject(Type type, Entity entity, IDataRecord record, IReadOnlyCollection<ForeignEntityInfo> foreignEntityInfos = null, int startFieldIndex = 0)
        {
            var entry = Activator.CreateInstance(type);
            var endFieldIndex = startFieldIndex + entity.Properties.Count(p => !p.IsExcluded);
            var skippedLocalProperties = new List<Property>();
            for (var i = startFieldIndex; i < endFieldIndex; i++)
            {
                var value = record.GetValue(i);
                if (value == DBNull.Value) value = null; // Use c# null
                var colName = record.GetName(i);

                var prop = entity.Properties.FirstOrDefault(p => p.ColumnName == colName);
                if (prop != null && !prop.ExistsOnEntityType && value == null) skippedLocalProperties.Add(prop); // related entity to skip
                if (prop == null || !prop.ExistsOnEntityType) continue;

                if (prop.IsEnum) value = Enum.ToObject(prop.Type, value ?? 0); // Map db int back to enum type
                
                prop.PropertyInfo.SetValue(entry, value);
            }

            if (foreignEntityInfos == null) return entry;
            
            foreach (var foreignEntityInfo in foreignEntityInfos)
            {
                var skipManyToManyReading = false;
                if (foreignEntityInfo.NavigationEntity != null) // Skip intermediary table cols in result set
                {
                    var navEntityEndIndex = endFieldIndex + foreignEntityInfo.NavigationEntity.Properties.Count(p => !p.IsExcluded);
                    for (var i = endFieldIndex; i < navEntityEndIndex; i++) // If intermediate table pks are null there is no related entity, so skip
                    {
                        if (record.GetValue(i) != DBNull.Value) continue;
                        
                        skipManyToManyReading = true;
                        break;
                    }

                    endFieldIndex = navEntityEndIndex;
                }

                object foreignObject;
                if (skipManyToManyReading || foreignEntityInfo.ForeignKeyInfos.Select(fkInfo => fkInfo.LocalProperty)
                    .Intersect(skippedLocalProperties).Any())
                {
                    foreignObject = null;
                }
                else
                {
                    foreignObject = DataRecordToObject(foreignEntityInfo.ForeignEntity.Type, foreignEntityInfo.ForeignEntity, record,
                        startFieldIndex: endFieldIndex);
                }

                endFieldIndex += foreignEntityInfo.ForeignEntity.Properties.Count(p => !p.IsExcluded);

                var targetType = foreignEntityInfo.ForeignEntityProperty.Type;
                if (Util.IsCollectionOfType(targetType, typeof(RelatedCollection<>)))
                {
                    var targetCollection = Activator.CreateInstance(targetType);

                    if (foreignObject != null)
                    {
                        targetCollection.GetType().GetMethod("Add")?.Invoke(targetCollection, new []{ foreignObject });
                    }
                    
                    foreignEntityInfo.ForeignEntityProperty.PropertyInfo.SetValue(entry, targetCollection);
                }
                else
                {
                    foreignEntityInfo.ForeignEntityProperty.PropertyInfo.SetValue(entry, foreignObject);
                }
            }

            return entry;
        }

        /// <summary>
        /// Compares to entity objects by their primary key values.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="obj1"></param>
        /// <param name="obj2"></param>
        /// <returns></returns>
        private static bool CompareObjectsByPks(Entity entity, object obj1, object obj2)
        {
            return entity.PrimaryKeys.All(pkProp => pkProp.PropertyInfo.GetValue(obj1).Equals(pkProp.PropertyInfo.GetValue(obj2)));
        }

        #endregion

        public void Dispose()
        {
            Connection?.Dispose();
        }
    }
}