using System;
using AbMapper.Core.Interfaces;
using Serilog.Events;

namespace AbMapper.DataAdapters.Postgresql
{
    /// <summary>
    /// Implements postgresql data source config.
    /// </summary>
    public class PostgresqlDataSourceConfig: IDataSourceConfig
    {
        /// <summary>
        /// Database connection string.
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Type of the datasource to be used with this config. AbMapper uses this to get an instance of the datasource.
        /// </summary>
        public Type DataSourceType { get; } = typeof(PostgresqlDataSource);

        /// <summary>
        /// If true, columns that are removed from entity classes are also dropped from the datasource.
        /// (results in data loss)
        /// </summary>
        public bool DropRemovedColumns { get; set; } = true;

        /// <summary>
        /// Completely rebuild the datasource structure on AbMapper startup. Used for development purposes.
        /// </summary>
        public bool WipeDatabaseOnStartup { get; set; }

        /// <summary>
        /// Enables caching for entity instances.
        /// </summary>
        public bool EnableCaching { get; set; } = true;

        /// <summary>
        /// AbMapper log level.
        /// </summary>
        public LogEventLevel LogLevel { get; set; } = LogEventLevel.Information;
    }
}