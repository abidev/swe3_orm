using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AbMapper.Core.Exceptions;
using AbMapper.Core.Interfaces;

namespace AbMapper.DataAdapters.Postgresql.QueryBuilders
{
    /// <summary>
    /// Builds an insert data sql query string using prepared statements.
    /// </summary>
    public class InsertQuery: IQueryBuilder
    {
        private StringBuilder StatementBuilder { get; }
        private bool _colAdded;
        private bool _finished;
        
        private readonly List<string> _dataEntries = new List<string>();
        private IEnumerable<string> _pkCols = new List<string>();

        private string _returningColumn = null;
        private InsertQueryConflictBehaviour? _conflictBehaviour = null;

        /// <summary>
        /// Sets the table to insert into.
        /// </summary>
        /// <param name="tableName"></param>
        public InsertQuery(string tableName)
        {
            StatementBuilder = new StringBuilder();
            StatementBuilder.Append("INSERT INTO ");
            StatementBuilder.Append($"{tableName} (");
        }

        /// <summary>
        /// Adds a column to the insert query, also adds a prepared statement param to the string (using the column name)
        /// </summary>
        /// <param name="colName">Name of the column and prepared statement param</param>
        /// <exception cref="AbQueryBuilderException">Thrown if the query is already finished</exception>
        public InsertQuery AddData(string colName)
        {
            // if (_colAdded) StatementBuilder.Append(", ");
            if (_finished) throw new AbQueryBuilderException("This insert query has already been finished");

            _dataEntries.Add(colName);
            
            return this;
        }

        /// <summary>
        /// Sets the column that will be returned using the returning keyword.
        /// </summary>
        /// <param name="colName">Column to return after statement execution. (as scalar)</param>
        /// <returns></returns>
        /// <exception cref="AbQueryBuilderException">Thrown if the query is already finished</exception>
        public InsertQuery SetReturningColumn(string colName)
        {
            if (_finished) throw new AbQueryBuilderException("This insert query has already been finished");

            _returningColumn = colName;

            return this;
        }

        /// <summary>
        /// Sets behaviour to apply if inserted data conflicts with existing data.
        /// See: https://www.postgresql.org/docs/current/static/sql-insert.html#SQL-ON-CONFLICT
        /// </summary>
        /// <param name="behaviour">Behaviour to apply</param>
        /// <param name="pkCols">Primary key cols of this tables, used for on conflict behaviour.</param>
        /// <returns></returns>
        /// <exception cref="AbQueryBuilderException">Thrown if the query is already finished</exception>
        public InsertQuery UseConflictBehaviour(InsertQueryConflictBehaviour behaviour, IEnumerable<string> pkCols)
        {
            if (_finished) throw new AbQueryBuilderException("This insert query has already been finished");

            _conflictBehaviour = behaviour;
            _pkCols = pkCols;

            return this;
        }

        /// <summary>
        /// Finishes the SQL query string. After Finish is called no modifications the the query can be made.
        /// Can be called multiple times.
        /// </summary>
        public string Finish()
        {
            if (_finished) return StatementBuilder.ToString();
            
            var valuesBuilder = new StringBuilder(") VALUES (");
            var updateColumnsBuilder = new StringBuilder("SET ");
            
            foreach (var dataEntry in _dataEntries)
            {
                if (_colAdded)
                {
                    StatementBuilder.Append(",");
                    valuesBuilder.Append(",");
                    updateColumnsBuilder.Append(",");
                }

                StatementBuilder.Append(dataEntry);
                valuesBuilder.Append($"@{dataEntry}");
                updateColumnsBuilder.Append($"{dataEntry}=@{dataEntry}");

                _colAdded = true;
            }

            StatementBuilder.Append(valuesBuilder);
            StatementBuilder.Append(")");

            if (_conflictBehaviour != null)
            {
                StatementBuilder.Append($" ON CONFLICT ({string.Join(',', _pkCols)})");
                switch (_conflictBehaviour)
                {
                    case InsertQueryConflictBehaviour.DoNothing:
                        StatementBuilder.Append("DO NOTHING");
                        break;
                    case InsertQueryConflictBehaviour.Update:
                        StatementBuilder.Append("DO UPDATE ");
                        StatementBuilder.Append(updateColumnsBuilder);
                        break;
                }
            }
                
            if (_returningColumn != null)
            {
                StatementBuilder.Append($" RETURNING {_returningColumn}");
            }
                
            StatementBuilder.Append(";");
            _finished = true;

            return StatementBuilder.ToString();
        }
    }

    public enum InsertQueryConflictBehaviour
    {
        DoNothing,
        Update
    }
}