using AbMapper.Core.Interfaces;

namespace AbMapper.DataAdapters.Postgresql.QueryBuilders
{
    /// <summary>
    /// Builds a drop table sql query string.
    /// </summary>
    public class DropTableQuery: IQueryBuilder
    {
        private string _tableName;
        
        /// <summary>
        /// Sets the table to drop.
        /// </summary>
        /// <param name="tableName"></param>
        public DropTableQuery(string tableName)
        {
            _tableName = tableName;
        }

        /// <summary>
        /// Finishes the SQL query string. After Finish is called no modifications the the query can be made.
        /// Can be called multiple times.
        /// </summary>
        public string Finish()
        {
            return $"DROP TABLE {_tableName} CASCADE;";
        }
    }
}