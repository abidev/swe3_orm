using System.Text;
using AbMapper.Core.Exceptions;
using AbMapper.Core.Interfaces;

namespace AbMapper.DataAdapters.Postgresql.QueryBuilders
{
    public class DeleteQuery: IQueryBuilder
    {
        private StringBuilder StatementBuilder { get; }
        private StringBuilder WhereStatementBuilder { get; }
        private bool _colAdded;
        private bool _finished;

        public DeleteQuery(string tableName)
        {
            StatementBuilder = new StringBuilder();
            WhereStatementBuilder = new StringBuilder();
            StatementBuilder.Append("DELETE FROM ");
            StatementBuilder.Append($"{tableName}");
        }

        /// <summary>
        /// Adds a primary key column to query to limit deleted data.
        /// Also adds prepared statement param with name matching col name.
        /// </summary>
        /// <param name="colName"></param>
        /// <returns></returns>
        public DeleteQuery AddPkColumn(string colName)
        {
            if (_finished) throw new AbQueryBuilderException("This delete query has already been finished");

            WhereStatementBuilder.Append(!_colAdded ? " WHERE " : " AND ");
            WhereStatementBuilder.Append($"{colName} = @{colName}");
            
            _colAdded = true;
            return this;
        }
        
        public string Finish()
        {
            if (!_finished)
            {
                StatementBuilder.Append(WhereStatementBuilder);
                StatementBuilder.Append(";");
                _finished = true;
            }

            return StatementBuilder.ToString();
        }
    }
}