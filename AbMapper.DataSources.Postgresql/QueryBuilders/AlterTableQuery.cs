using System;
using System.Collections.Generic;
using System.Text;
using AbMapper.Core.Exceptions;
using AbMapper.Core.Interfaces;
using AbMapper.Core.Models;

namespace AbMapper.DataAdapters.Postgresql.QueryBuilders
{
    /// <summary>
    /// Builds an alter table sql query string.
    /// </summary>
    public class AlterTableQuery: IQueryBuilder
    {
        private StringBuilder StatementBuilder { get; } = new StringBuilder();
        private bool _actionRegistered;
        private bool _finished;

        public bool HasActions => _actionRegistered;

        /// <summary>
        /// Sets the table to alter.
        /// </summary>
        /// <param name="tableName"></param>
        public AlterTableQuery(string tableName)
        {
            StatementBuilder.Append($"ALTER TABLE {tableName}");
        }

        /// <summary>
        /// Adds a column to the alter table statement.
        /// </summary>
        /// <param name="columnName">Name of the column</param>
        /// <param name="type">C# type of the column</param>
        /// <param name="modifiers">Modifiers to be applied to the column</param>
        public AlterTableQuery AddColumn(string columnName, Type type, string modifiers = null)
        {
            return AddColumn(columnName, PostgresqlTypeMapper.GetType(type), modifiers);
        }
        
        /// <summary>
        /// Adds a column to the alter table statement.
        /// </summary>
        /// <param name="columnName">Name of the column</param>
        /// <param name="type">Postgresql type string</param>
        /// <param name="modifiers">Modifiers to be applied to the column</param>
        public AlterTableQuery AddColumn(string columnName, string type, string modifiers = null)
        {
            if (_actionRegistered) StatementBuilder.Append(",");
            if (_finished) throw new AbQueryBuilderException("This alter table query has already been finished");

            StatementBuilder.Append(" ADD COLUMN ").Append(columnName).Append($" {type}");

            if (!String.IsNullOrEmpty(modifiers)) StatementBuilder.Append($" {modifiers}");
            
            Console.WriteLine(StatementBuilder.ToString());
            
            _actionRegistered = true;
            
            return this;
        }

        /// <summary>
        /// Adds a foreign key constraint to the alter table statement.
        /// </summary>
        /// <param name="cols">Enumerable of cols that are part of the fk</param>
        /// <param name="foreignTableName">Name of the related table</param>
        /// <param name="foreignTableCols">Enumerable of the related cols</param>
        /// <param name="deleteBehaviour">Delete behaviour to use for this fk</param>
        /// <exception cref="AbQueryBuilderException">Thrown if the query is already finished</exception>
        public AlterTableQuery AddForeignKeyConstraint(IEnumerable<string> cols, string foreignTableName, IEnumerable<string> foreignTableCols, DeleteBehaviour? deleteBehaviour = DeleteBehaviour.NoAction)
        {
            if (_actionRegistered) StatementBuilder.Append(",");
            if (_finished) throw new AbQueryBuilderException("This alter table query has already been finished");

            StatementBuilder.Append($" ADD FOREIGN KEY ({string.Join(',', cols)})");
            StatementBuilder.Append($" REFERENCES {foreignTableName} ({string.Join(',', foreignTableCols)})");
            
            switch (deleteBehaviour)
            {
                case DeleteBehaviour.Cascade:
                    StatementBuilder.Append(" ON DELETE CASCADE");
                    break;
                case DeleteBehaviour.Restrict:
                    StatementBuilder.Append(" ON DELETE RESTRICT");
                    break;
                case DeleteBehaviour.SetNull:
                    StatementBuilder.Append(" ON DELETE SET NULL");
                    break;
            }
            
            _actionRegistered = true;
            
            return this;
        }

        /// <summary>
        /// Adds a drop column statement.
        /// </summary>
        /// <param name="columnName">Name of the column to drop</param>
        /// <exception cref="AbQueryBuilderException">Thrown if the query is already finished</exception>
        public AlterTableQuery DropColumn(string columnName)
        {
            if (_actionRegistered) StatementBuilder.Append(",");
            if (_finished) throw new AbQueryBuilderException("This alter table query has already been finished");
            
            StatementBuilder.Append(" DROP COLUMN ").Append(columnName);

            _actionRegistered = true;

            return this;
        }

        /// <summary>
        /// Finishes the SQL query string. After Finish is called no modifications the the query can be made.
        /// Can be called multiple times.
        /// </summary>
        public string Finish()
        {
            if (!_actionRegistered) throw new AbQueryBuilderException("Cannot finish query without actions");
            
            if (!_finished)
            {
                StatementBuilder.Append(";");
                _finished = true;
            }

            return StatementBuilder.ToString();
        }
    }
}