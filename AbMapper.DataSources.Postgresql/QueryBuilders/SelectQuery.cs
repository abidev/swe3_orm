using System;
using System.Collections.Generic;
using System.Text;
using AbMapper.Core.Exceptions;
using AbMapper.Core.Interfaces;
using AbMapper.Core.Models;

namespace AbMapper.DataAdapters.Postgresql.QueryBuilders
{
    /// <summary>
    /// Builds a select sql query string.
    /// </summary>
    public class SelectQuery: IQueryBuilder
    {
        private StringBuilder StatementBuilder { get; } = new StringBuilder();
        private StringBuilder SelectedColumnsBuilder { get; } = new StringBuilder();
        private StringBuilder WhereStatementsBuilder { get; } = new StringBuilder();
        private StringBuilder JoinStatementsBuilder { get; } = new StringBuilder();
        
        private readonly string _tableName;
        private bool _colSelected;
        private bool _whereStmtAdded;

        private const int GuidLength = 8;
        
        public string QueryIdentity { get; } = $"t_{Guid.NewGuid().ToString("N").Substring(0, GuidLength)}";

        private bool _finished;

        private List<string> _dataEntries = new List<string>();

        private int? _limit;
        private int? _skip;
        
        private string _orderByCol = null;
        private OrderByDirection _orderByDirection = OrderByDirection.Ascending;

        /// <summary>
        /// Sets the table to select from.
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="cols">Comma separated list of columns</param>
        public SelectQuery(string tableName, string cols = null)
        {
            _tableName = tableName;
            StatementBuilder.Append("SELECT ");
            
            if (cols == null) return;
            
            SelectedColumnsBuilder.Append(cols);
            _colSelected = true;
        }

        /// <summary>
        /// Overwrites the initial column select list.
        /// </summary>
        /// <param name="cols"></param>
        public SelectQuery SetColumns(string cols)
        {
            if (_finished) throw new AbQueryBuilderException("This select query has already been finished");

            SelectedColumnsBuilder.Clear();
            SelectedColumnsBuilder.Append(cols);
            _colSelected = true;

            return this;
        }

        /// <summary>
        /// Adds a column to the selection list.
        /// </summary>
        /// <param name="column"></param>
        /// <exception cref="AbQueryBuilderException">Thrown if the query is already finished</exception>
        public SelectQuery AddColumn(string column)
        {
            if (_colSelected) SelectedColumnsBuilder.Append(", ");
            if (_finished) throw new AbQueryBuilderException("This select query has already been finished");

            SelectedColumnsBuilder.Append(column);
            _colSelected = true;
            
            return this;
        }

        /// <summary>
        /// Adds a where statement to the select query. Also adds a prepared statement param to the string (using the column name).
        /// </summary>
        /// <returns></returns>
        public SelectQuery AddConstantColumnConstraint(string column, string operation, bool addFromIdentityToLocalCols = true)
        {
            WhereStatementsBuilder.Append(_whereStmtAdded ? " AND " : " WHERE ");
            if (_finished) throw new AbQueryBuilderException("This select query has already been finished");

            if (addFromIdentityToLocalCols) WhereStatementsBuilder.Append($"{QueryIdentity}.");
            WhereStatementsBuilder.Append($"{column} {operation} @{column}");

            _whereStmtAdded = true;
            return this;
        }

        /// <summary>
        /// Adds a custom where statement to the query, null cols are replaced by prepared statements.
        /// </summary>
        /// <param name="col1"></param>
        /// <param name="operation"></param>
        /// <param name="col2"></param>
        /// <returns>Prepared statement name or null</returns>
        public string AddWhereStatement(string col1, string operation, string col2)
        {
            WhereStatementsBuilder.Append(_whereStmtAdded ? " AND " : " WHERE ");
            if (_finished) throw new AbQueryBuilderException("This select query has already been finished");
            
            string preparedStatementName = null;
            
            preparedStatementName = AddColToWhereStatement(col1);
            
            WhereStatementsBuilder.Append($" {operation} ");

            var col2Result = AddColToWhereStatement(col2);
            if (preparedStatementName != null && col2Result != null)
                throw new AbQueryBuilderException(
                    "The AbMapper select query builder cannot process multiple prepared statements in one where expression");

            _whereStmtAdded = true;
            return preparedStatementName ?? col2Result;
        }

        private string AddColToWhereStatement(string col)
        {
            if (col == null)
            {
                var colName = $"p_{Guid.NewGuid().ToString("N").Substring(0, GuidLength)}";
                WhereStatementsBuilder.Append($"@{colName}");
                return colName;
            }

            WhereStatementsBuilder.Append(col);
            return null;
        }

        /// <summary>
        /// Adds a join statement to the select query.
        /// </summary>
        /// <param name="table"></param>
        /// <param name="localCols"></param>
        /// <param name="foreignCols"></param>
        /// <param name="addFromIdentityToLocalCols"></param>
        /// <returns>Joined table's alias</returns>
        /// <exception cref="AbQueryBuilderException">Thrown if the query is already finished</exception>
        public string AddJoin(string table, string[] localCols, string[] foreignCols, bool addFromIdentityToLocalCols = true)
        {
            if (localCols.Length != foreignCols.Length) throw new AbQueryBuilderException("Local and foreign column count mismatch");
            if (_finished) throw new AbQueryBuilderException("This select query has already been finished");

            var joinedTableIdentity = $"c_{Guid.NewGuid().ToString("N").Substring(0, GuidLength)}";
            JoinStatementsBuilder.Append($" LEFT JOIN {table} {joinedTableIdentity} ON");

            for (int i = 0; i < localCols.Length; i++)
            {
                JoinStatementsBuilder.Append(" ");
                if (addFromIdentityToLocalCols) JoinStatementsBuilder.Append($"{QueryIdentity}.");
                JoinStatementsBuilder.Append($"{localCols[i]} = {joinedTableIdentity}.{foreignCols[i]}");
            }
            
            return joinedTableIdentity;
        }

        /// <summary>
        /// Sets select query returned row number limit.
        /// </summary>
        public void SetLimit(int? limit)
        {
            if (_finished) throw new AbQueryBuilderException("This select query has already been finished");
            _limit = limit;
        }
        
        /// <summary>
        /// Sets select query skipped row count.
        /// </summary>
        public void SetSkipCount(int? skip)
        {
            if (_finished) throw new AbQueryBuilderException("This select query has already been finished");
            _skip = skip;
        }

        /// <summary>
        /// Sets the column and direction to order by.
        /// </summary>
        /// <param name="col"></param>
        /// <param name="direction"></param>
        public void SetOrderByInfo(string col, OrderByDirection direction = OrderByDirection.Ascending)
        {
            if (_finished) throw new AbQueryBuilderException("This select query has already been finished");
            _orderByCol = col;
            _orderByDirection = direction;
        }

        /// <summary>
        /// Finishes the SQL query string. After Finish is called no modifications the the query can be made.
        /// Can be called multiple times.
        /// </summary>
        public string Finish()
        {
            if (_finished) return StatementBuilder.ToString();
            
            StatementBuilder.Append(SelectedColumnsBuilder.ToString());
            StatementBuilder.Append($" FROM {_tableName} {QueryIdentity}");
            StatementBuilder.Append(JoinStatementsBuilder.ToString());
            StatementBuilder.Append(WhereStatementsBuilder.ToString());
            if (!string.IsNullOrEmpty(_orderByCol))
            {
                StatementBuilder.Append($" ORDER BY {QueryIdentity}.{_orderByCol} ");
                StatementBuilder.Append(_orderByDirection == OrderByDirection.Ascending ? "ASC" : "DESC");
            }
            if (_limit != null) StatementBuilder.Append($" LIMIT {_limit}");
            if (_skip != null) StatementBuilder.Append($" OFFSET {_skip}");
            StatementBuilder.Append(";");
            _finished = true;

            return StatementBuilder.ToString();
        }
    }
}