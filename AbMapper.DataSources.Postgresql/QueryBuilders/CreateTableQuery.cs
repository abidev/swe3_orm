using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AbMapper.Core.Exceptions;
using AbMapper.Core.Interfaces;
using AbMapper.DataAdapters.Postgresql.Models;

namespace AbMapper.DataAdapters.Postgresql.QueryBuilders
{
    /// <summary>
    /// Builds a create table sql query string.
    /// </summary>
    public class CreateTableQuery: IQueryBuilder
    {
        private StringBuilder StatementBuilder { get; }
        private bool _colRegistered;
        private bool _finished;
        
        private readonly List<string> _primaryKeyColumns = new List<string>();
        private readonly Dictionary<string, List<KeyValuePair<string, string>>> _foreignKeyCollections = new Dictionary<string, List<KeyValuePair<string, string>>>();

        /// <summary>
        /// Sets the table name to create.
        /// </summary>
        /// <param name="tableName">Name of the table to be created</param>
        /// <param name="ifNotExists">If true, the if not exists modifier is applied to the query</param>
        public CreateTableQuery(string tableName, bool ifNotExists = false)
        {
            StatementBuilder = new StringBuilder();
            StatementBuilder.Append("CREATE TABLE ");
            if (ifNotExists) StatementBuilder.Append("IF NOT EXISTS ");
            StatementBuilder.Append($"{tableName} (");
        }

        /// <summary>
        /// Adds a column to the create table statement.
        /// </summary>
        /// <param name="columnName">Name of the column</param>
        /// <param name="type">C# type of the column</param>
        /// <param name="modifiers">Modifiers to be applied to the column</param>
        /// <param name="isPrimary">If true, this column is added to the list of primary keys</param>
        /// <param name="foreignKeyInfo">If this column references another table and column(s), this information is to be supplied here</param>
        public CreateTableQuery AddColumn(string columnName, Type type, string modifiers = null, bool isPrimary = false, DbForeignKeyInfo foreignKeyInfo = null)
        {
            return AddColumn(columnName, PostgresqlTypeMapper.GetType(type), modifiers, isPrimary, foreignKeyInfo);
        }
        
        /// <summary>
        /// Adds a column to the create table statement.
        /// </summary>
        /// <param name="columnName">Name of the column</param>
        /// <param name="type">Postgresql type string of the column</param>
        /// <param name="modifiers">Modifiers to be applied to the column</param>
        /// <param name="isPrimary">If true, this column is added to the list of primary keys</param>
        /// <param name="foreignKeyInfo">If this column references another table and column(s), this information is to be supplied here</param>
        public CreateTableQuery AddColumn(string columnName, string type, string modifiers = null, bool isPrimary = false, DbForeignKeyInfo foreignKeyInfo = null)
        {
            if (_colRegistered) StatementBuilder.Append(", ");
            if (_finished) throw new AbQueryBuilderException("This create table query has already been finished");

            StatementBuilder.Append(columnName).Append($" {type}");

            if (!String.IsNullOrEmpty(modifiers)) StatementBuilder.Append($" {modifiers}");
            if (isPrimary) _primaryKeyColumns.Add(columnName);
            if (foreignKeyInfo != null) AddForeignKeyColumn(columnName, foreignKeyInfo);
            
            _colRegistered = true;
            return this;
        }

        /// <summary>
        /// Adds an already added column to the list of foreign key columns.
        /// </summary>
        /// <param name="colName">Name of the column</param>
        /// <param name="info">Foreign key information</param>
        private void AddForeignKeyColumn(string colName, DbForeignKeyInfo info)
        {
            var keyValuePair = new KeyValuePair<string, string>(colName, info.ForeignColumnName);
            if (_foreignKeyCollections.ContainsKey(info.ForeignTableName)) _foreignKeyCollections[info.ForeignTableName].Add(keyValuePair);
            else _foreignKeyCollections.Add(info.ForeignTableName, new List<KeyValuePair<string, string>> {keyValuePair});
        }

        /// <summary>
        /// Finishes the SQL query string. After Finish is called no modifications the the query can be made.
        /// Can be called multiple times.
        /// </summary>
        public string Finish()
        {
            if (!_colRegistered) throw new AbQueryBuilderException("Cannot finish query without columns");
            
            if (!_finished)
            {
                foreach (var entry in _foreignKeyCollections)
                {
                    if (entry.Value != null && entry.Value.Count > 0)
                    {
                        StatementBuilder.Append($", FOREIGN KEY ({String.Join(',', entry.Value.Select(v => v.Key))})");
                        StatementBuilder.Append($" REFERENCES {entry.Key} ({String.Join(',', entry.Value.Select(v => v.Value))})");
                    }
                }
                StatementBuilder.Append($", PRIMARY KEY ({string.Join(',', _primaryKeyColumns)}));");
                _finished = true;
            }
            
            return StatementBuilder.ToString();
        }
    }
}