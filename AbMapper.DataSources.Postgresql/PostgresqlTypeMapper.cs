using System;
using System.Collections.Generic;
using AbMapper.Core.Exceptions;
using Npgsql;
using Serilog;

namespace AbMapper.DataAdapters.Postgresql
{
    /// <summary>
    /// Maps c# types to postgresql type strings.
    /// </summary>
    public static class PostgresqlTypeMapper
    {
        /// <summary>
        /// Holds db types, initialized with custom type mappings.
        /// </summary>
        private static IDictionary<Type, string> TypeToDbTypeMap { get; } = new Dictionary<Type, string>
        {   // Custom type mappings
            { typeof(int?), "integer" },
            { typeof(bool?), "boolean" },
            { typeof(double?), "double precision" },
            { typeof(decimal?), "numeric" },
            { typeof(DateTime), "timestamp" }
        };
        private static bool _built;

        /// <summary>
        /// Builds type map from Npgsql internal type map.
        /// </summary>
        private static void RebuildTypeMap()
        {
            foreach (var mapping in NpgsqlConnection.GlobalTypeMapper.Mappings)
            {
                if (mapping.ClrTypes.Length > 0 && !TypeToDbTypeMap.ContainsKey(mapping.ClrTypes[0])) 
                    TypeToDbTypeMap.Add(mapping.ClrTypes[0], mapping.PgTypeName);
            }
            _built = true;
        }

        /// <summary>
        /// Gets the postgresql db type as string for the given c# type.
        /// </summary>
        /// <param name="type">Type to convert to db type.</param>
        /// <returns>Postgresql db type.</returns>
        /// <exception cref="AbTypeMapperException">Thrown if no db type exists for given c# type.</exception>
        public static string GetType(Type type)
        {
            if (!_built)
                RebuildTypeMap();
            
            if (!TypeToDbTypeMap.TryGetValue(type, out string dbType))
            {
                Log.Logger.Warning("{Class}: No postgres type found for type {Type}", nameof(PostgresqlTypeMapper), type);
                throw new AbTypeMapperException($"No postgres type found for type {type}");
            }

            return dbType;
        }
    }
}