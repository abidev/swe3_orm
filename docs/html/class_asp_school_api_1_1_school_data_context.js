var class_asp_school_api_1_1_school_data_context =
[
    [ "SchoolDataContext", "class_asp_school_api_1_1_school_data_context.html#a9c52cbbee079fe085c354a7a19b78937", null ],
    [ "Classes", "class_asp_school_api_1_1_school_data_context.html#a13c2300926f6ca57bc8f5a4927769722", null ],
    [ "Courses", "class_asp_school_api_1_1_school_data_context.html#a5ea6edacffcfbb5a36568e32ae63e14e", null ],
    [ "Students", "class_asp_school_api_1_1_school_data_context.html#afd5a5f9a794aba745acab2cd78f47b10", null ],
    [ "Teachers", "class_asp_school_api_1_1_school_data_context.html#af3703cb7d76ed3eb6a16bab9938b9b3f", null ]
];