var hierarchy =
[
    [ "AbMapper.Tests.AbMapperTests", "class_ab_mapper_1_1_tests_1_1_ab_mapper_tests.html", null ],
    [ "Attribute", null, [
      [ "AbMapper.Core.Attributes.ColumnNameAttribute", "class_ab_mapper_1_1_core_1_1_attributes_1_1_column_name_attribute.html", null ],
      [ "AbMapper.Core.Attributes.DeleteBehaviourAttribute", "class_ab_mapper_1_1_core_1_1_attributes_1_1_delete_behaviour_attribute.html", null ],
      [ "AbMapper.Core.Attributes.ExcludedAttribute", "class_ab_mapper_1_1_core_1_1_attributes_1_1_excluded_attribute.html", null ],
      [ "AbMapper.Core.Attributes.ForeignKeyAttribute", "class_ab_mapper_1_1_core_1_1_attributes_1_1_foreign_key_attribute.html", null ],
      [ "AbMapper.Core.Attributes.NotNullAttribute", "class_ab_mapper_1_1_core_1_1_attributes_1_1_not_null_attribute.html", null ],
      [ "AbMapper.Core.Attributes.PrimaryKeyAttribute", "class_ab_mapper_1_1_core_1_1_attributes_1_1_primary_key_attribute.html", null ],
      [ "AbMapper.Core.Attributes.SerialAttribute", "class_ab_mapper_1_1_core_1_1_attributes_1_1_serial_attribute.html", null ]
    ] ],
    [ "AbMapper.Sample.Models.Class", "class_ab_mapper_1_1_sample_1_1_models_1_1_class.html", null ],
    [ "AbMapper.Tests.Models.Class", "class_ab_mapper_1_1_tests_1_1_models_1_1_class.html", null ],
    [ "AbMapper.DataAdapters.Postgresql.Models.Column", "class_ab_mapper_1_1_data_adapters_1_1_postgresql_1_1_models_1_1_column.html", null ],
    [ "ControllerBase", null, [
      [ "AspSchoolApi.Controllers.StudentController", "class_asp_school_api_1_1_controllers_1_1_student_controller.html", null ]
    ] ],
    [ "AbMapper.Sample.Models.Course", "class_ab_mapper_1_1_sample_1_1_models_1_1_course.html", null ],
    [ "AbMapper.Tests.Models.Course", "class_ab_mapper_1_1_tests_1_1_models_1_1_course.html", null ],
    [ "AbMapper.Core.DataContext", "class_ab_mapper_1_1_core_1_1_data_context.html", [
      [ "AbMapper.Sample.CustomDataContext", "class_ab_mapper_1_1_sample_1_1_custom_data_context.html", null ],
      [ "AbMapper.Tests.TestContext", "class_ab_mapper_1_1_tests_1_1_test_context.html", null ],
      [ "AspSchoolApi.SchoolDataContext", "class_asp_school_api_1_1_school_data_context.html", null ]
    ] ],
    [ "AbMapper.DataAdapters.Postgresql.Models.DbForeignKeyInfo", "class_ab_mapper_1_1_data_adapters_1_1_postgresql_1_1_models_1_1_db_foreign_key_info.html", null ],
    [ "AbMapper.Core.Reflection.Entity", "class_ab_mapper_1_1_core_1_1_reflection_1_1_entity.html", null ],
    [ "Exception", null, [
      [ "AbMapper.Core.Exceptions.AbCacheException", "class_ab_mapper_1_1_core_1_1_exceptions_1_1_ab_cache_exception.html", null ],
      [ "AbMapper.Core.Exceptions.AbCollectionPrimaryKeyCountException", "class_ab_mapper_1_1_core_1_1_exceptions_1_1_ab_collection_primary_key_count_exception.html", null ],
      [ "AbMapper.Core.Exceptions.AbDataSourceException", "class_ab_mapper_1_1_core_1_1_exceptions_1_1_ab_data_source_exception.html", null ],
      [ "AbMapper.Core.Exceptions.AbInvalidWhereExpressionException", "class_ab_mapper_1_1_core_1_1_exceptions_1_1_ab_invalid_where_expression_exception.html", null ],
      [ "AbMapper.Core.Exceptions.AbMapperException", "class_ab_mapper_1_1_core_1_1_exceptions_1_1_ab_mapper_exception.html", null ],
      [ "AbMapper.Core.Exceptions.AbNoDataSourceException", "class_ab_mapper_1_1_core_1_1_exceptions_1_1_ab_no_data_source_exception.html", null ],
      [ "AbMapper.Core.Exceptions.AbQueryBuilderException", "class_ab_mapper_1_1_core_1_1_exceptions_1_1_ab_query_builder_exception.html", null ],
      [ "AbMapper.Core.Exceptions.AbTrackingException", "class_ab_mapper_1_1_core_1_1_exceptions_1_1_ab_tracking_exception.html", null ],
      [ "AbMapper.Core.Exceptions.AbTypeMapperException", "class_ab_mapper_1_1_core_1_1_exceptions_1_1_ab_type_mapper_exception.html", null ]
    ] ],
    [ "AbMapper.Core.Reflection.ForeignEntityInfo", "class_ab_mapper_1_1_core_1_1_reflection_1_1_foreign_entity_info.html", null ],
    [ "AbMapper.Core.Reflection.ForeignKeyInfo", "class_ab_mapper_1_1_core_1_1_reflection_1_1_foreign_key_info.html", null ],
    [ "AbMapper.Core.Interfaces.IDataSource", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source.html", [
      [ "AbMapper.DataAdapters.Postgresql.PostgresqlDataSource", "class_ab_mapper_1_1_data_adapters_1_1_postgresql_1_1_postgresql_data_source.html", null ]
    ] ],
    [ "AbMapper.Core.Interfaces.IDataSourceConfig", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source_config.html", [
      [ "AbMapper.DataAdapters.Postgresql.PostgresqlDataSourceConfig", "class_ab_mapper_1_1_data_adapters_1_1_postgresql_1_1_postgresql_data_source_config.html", null ]
    ] ],
    [ "AbMapper.Core.Interfaces.IDbChange", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_db_change.html", [
      [ "AbMapper.Core.Models.DbChange", "class_ab_mapper_1_1_core_1_1_models_1_1_db_change.html", null ]
    ] ],
    [ "IDisposable", null, [
      [ "AbMapper.Core.EntityCache", "class_ab_mapper_1_1_core_1_1_entity_cache.html", null ],
      [ "AbMapper.DataAdapters.Postgresql.PostgresqlDataSource", "class_ab_mapper_1_1_data_adapters_1_1_postgresql_1_1_postgresql_data_source.html", null ]
    ] ],
    [ "AbMapper.Core.Interfaces.IQueryBuilder", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_query_builder.html", [
      [ "AbMapper.DataAdapters.Postgresql.QueryBuilders.AlterTableQuery", "class_ab_mapper_1_1_data_adapters_1_1_postgresql_1_1_query_builders_1_1_alter_table_query.html", null ],
      [ "AbMapper.DataAdapters.Postgresql.QueryBuilders.CreateTableQuery", "class_ab_mapper_1_1_data_adapters_1_1_postgresql_1_1_query_builders_1_1_create_table_query.html", null ],
      [ "AbMapper.DataAdapters.Postgresql.QueryBuilders.DeleteQuery", "class_ab_mapper_1_1_data_adapters_1_1_postgresql_1_1_query_builders_1_1_delete_query.html", null ],
      [ "AbMapper.DataAdapters.Postgresql.QueryBuilders.DropTableQuery", "class_ab_mapper_1_1_data_adapters_1_1_postgresql_1_1_query_builders_1_1_drop_table_query.html", null ],
      [ "AbMapper.DataAdapters.Postgresql.QueryBuilders.InsertQuery", "class_ab_mapper_1_1_data_adapters_1_1_postgresql_1_1_query_builders_1_1_insert_query.html", null ],
      [ "AbMapper.DataAdapters.Postgresql.QueryBuilders.SelectQuery", "class_ab_mapper_1_1_data_adapters_1_1_postgresql_1_1_query_builders_1_1_select_query.html", null ]
    ] ],
    [ "List", null, [
      [ "AbMapper.Core.Models.DbCollection< T >", "class_ab_mapper_1_1_core_1_1_models_1_1_db_collection.html", null ],
      [ "AbMapper.Core.Models.RelatedCollection< T >", "class_ab_mapper_1_1_core_1_1_models_1_1_related_collection.html", null ]
    ] ],
    [ "AbMapper.Core.MapperInitializer", "class_ab_mapper_1_1_core_1_1_mapper_initializer.html", null ],
    [ "AbMapper.Core.MapperManager", "class_ab_mapper_1_1_core_1_1_mapper_manager.html", null ],
    [ "AbMapper.Core.Models.OrderByInfo", "class_ab_mapper_1_1_core_1_1_models_1_1_order_by_info.html", null ],
    [ "AbMapper.Tests.Models.Parking", "class_ab_mapper_1_1_tests_1_1_models_1_1_parking.html", null ],
    [ "AbMapper.Sample.Models.Person", "class_ab_mapper_1_1_sample_1_1_models_1_1_person.html", [
      [ "AbMapper.Sample.Models.Student", "class_ab_mapper_1_1_sample_1_1_models_1_1_student.html", null ],
      [ "AbMapper.Sample.Models.Teacher", "class_ab_mapper_1_1_sample_1_1_models_1_1_teacher.html", null ]
    ] ],
    [ "AbMapper.Tests.Models.Person", "class_ab_mapper_1_1_tests_1_1_models_1_1_person.html", [
      [ "AbMapper.Tests.Models.Student", "class_ab_mapper_1_1_tests_1_1_models_1_1_student.html", null ],
      [ "AbMapper.Tests.Models.Teacher", "class_ab_mapper_1_1_tests_1_1_models_1_1_teacher.html", null ]
    ] ],
    [ "AbMapper.DataAdapters.Postgresql.PostgresqlInitializer", "class_ab_mapper_1_1_data_adapters_1_1_postgresql_1_1_postgresql_initializer.html", [
      [ "AbMapper.DataAdapters.Postgresql.PostgresqlDataSource", "class_ab_mapper_1_1_data_adapters_1_1_postgresql_1_1_postgresql_data_source.html", null ]
    ] ],
    [ "AbMapper.DataAdapters.Postgresql.PostgresqlTypeMapper", "class_ab_mapper_1_1_data_adapters_1_1_postgresql_1_1_postgresql_type_mapper.html", null ],
    [ "AbMapper.Sample.Program", "class_ab_mapper_1_1_sample_1_1_program.html", null ],
    [ "AspSchoolApi.Program", "class_asp_school_api_1_1_program.html", null ],
    [ "AbMapper.Core.Reflection.Property", "class_ab_mapper_1_1_core_1_1_reflection_1_1_property.html", null ],
    [ "AbMapper.Tests.SetupTests", "class_ab_mapper_1_1_tests_1_1_setup_tests.html", null ],
    [ "AspSchoolApi.Startup", "class_asp_school_api_1_1_startup.html", null ],
    [ "AbMapper.DataAdapters.Postgresql.Models.Table", "class_ab_mapper_1_1_data_adapters_1_1_postgresql_1_1_models_1_1_table.html", null ],
    [ "AbMapper.Core.Util", "class_ab_mapper_1_1_core_1_1_util.html", null ]
];