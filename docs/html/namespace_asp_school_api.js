var namespace_asp_school_api =
[
    [ "Controllers", "namespace_asp_school_api_1_1_controllers.html", "namespace_asp_school_api_1_1_controllers" ],
    [ "Program", "class_asp_school_api_1_1_program.html", "class_asp_school_api_1_1_program" ],
    [ "SchoolDataContext", "class_asp_school_api_1_1_school_data_context.html", "class_asp_school_api_1_1_school_data_context" ],
    [ "Startup", "class_asp_school_api_1_1_startup.html", "class_asp_school_api_1_1_startup" ]
];