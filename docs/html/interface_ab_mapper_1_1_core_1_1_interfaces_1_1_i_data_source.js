var interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source =
[
    [ "Begin", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source.html#a23aaffa8f20009c8c9ef644a8fdee8fd", null ],
    [ "Commit", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source.html#a925427c9a979c7dfdc0a9266400b3f01", null ],
    [ "GetAllAsync< T >", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source.html#ac279d456a77acfd53399d6cfbfc8bf55", null ],
    [ "GetAsync< T >", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source.html#aa16e6757d474a5b03ff4195597e35c6b", null ],
    [ "InitializeAsync", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source.html#a9b5ebf153e2b28f23289dd20ee7e8d61", null ],
    [ "InsertAsync", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source.html#ae2ac11c380984af13d8ca8155600f659", null ],
    [ "RemoveAsync", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source.html#adf2def9cca6c4b04f515c1c1e1ee0324", null ],
    [ "Rollback", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source.html#ae8da6b5a1646432dc30e5431123aee1f", null ],
    [ "SetupRelationshipsAsync", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source.html#a557de310d517f17f1dfbbdf4a38a1980", null ],
    [ "SyncEntityAsync", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source.html#a98114e15a1bf5ed7282e7208534fdd7b", null ]
];