var interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source_config =
[
    [ "ConnectionString", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source_config.html#a23988e641dd71e04a76c972218c41f84", null ],
    [ "DataSourceType", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source_config.html#a3e7cdc1f51165bd0fb3982083d724ef2", null ],
    [ "DropRemovedColumns", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source_config.html#a4606cce9150782236aa61b2dec48aab1", null ],
    [ "EnableCaching", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source_config.html#a7bcec6327fe47cf2e5896d051ac42d53", null ],
    [ "LogLevel", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source_config.html#af990860ab2682395a7305cd1e2b49c2c", null ],
    [ "WipeDatabaseOnStartup", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source_config.html#a105adc07f9117884f3b252186845b168", null ]
];