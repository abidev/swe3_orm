var dir_672ff90780f570d23169be4952ef4964 =
[
    [ "Attributes", "dir_bb4062ab653a0839008ff10b3302c89a.html", "dir_bb4062ab653a0839008ff10b3302c89a" ],
    [ "Exceptions", "dir_fcaa31e0ac3b3db682d208dcb85ffaed.html", "dir_fcaa31e0ac3b3db682d208dcb85ffaed" ],
    [ "Interfaces", "dir_e28d5d3e9270ad4bbe5cf493a18e46a9.html", "dir_e28d5d3e9270ad4bbe5cf493a18e46a9" ],
    [ "Models", "dir_b5ec8935c94f9604e35858a2e84eab2d.html", "dir_b5ec8935c94f9604e35858a2e84eab2d" ],
    [ "obj", "dir_25240b493d4bffedaa633f7f5dd302dc.html", "dir_25240b493d4bffedaa633f7f5dd302dc" ],
    [ "Reflection", "dir_44dcd74e91ee12ddfbe42c0f91e22e47.html", "dir_44dcd74e91ee12ddfbe42c0f91e22e47" ],
    [ "DataContext.cs", "_data_context_8cs.html", [
      [ "DataContext", "class_ab_mapper_1_1_core_1_1_data_context.html", "class_ab_mapper_1_1_core_1_1_data_context" ]
    ] ],
    [ "EntityCache.cs", "_entity_cache_8cs.html", [
      [ "EntityCache", "class_ab_mapper_1_1_core_1_1_entity_cache.html", "class_ab_mapper_1_1_core_1_1_entity_cache" ]
    ] ],
    [ "MapperInitializer.cs", "_mapper_initializer_8cs.html", [
      [ "MapperInitializer", "class_ab_mapper_1_1_core_1_1_mapper_initializer.html", "class_ab_mapper_1_1_core_1_1_mapper_initializer" ]
    ] ],
    [ "MapperManager.cs", "_mapper_manager_8cs.html", "_mapper_manager_8cs" ],
    [ "Util.cs", "_util_8cs.html", [
      [ "Util", "class_ab_mapper_1_1_core_1_1_util.html", "class_ab_mapper_1_1_core_1_1_util" ]
    ] ]
];