var namespace_ab_mapper_1_1_core_1_1_interfaces =
[
    [ "IDataSource", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source.html", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source" ],
    [ "IDataSourceConfig", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source_config.html", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source_config" ],
    [ "IDbChange", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_db_change.html", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_db_change" ],
    [ "IQueryBuilder", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_query_builder.html", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_query_builder" ]
];