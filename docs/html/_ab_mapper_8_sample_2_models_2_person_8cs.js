var _ab_mapper_8_sample_2_models_2_person_8cs =
[
    [ "Person", "class_ab_mapper_1_1_sample_1_1_models_1_1_person.html", "class_ab_mapper_1_1_sample_1_1_models_1_1_person" ],
    [ "Sex", "_ab_mapper_8_sample_2_models_2_person_8cs.html#aa0c8af0ac6c43d02de820d74dcf60596", [
      [ "Male", "_ab_mapper_8_sample_2_models_2_person_8cs.html#aa0c8af0ac6c43d02de820d74dcf60596a63889cfb9d3cbe05d1bd2be5cc9953fd", null ],
      [ "Female", "_ab_mapper_8_sample_2_models_2_person_8cs.html#aa0c8af0ac6c43d02de820d74dcf60596ab719ce180ec7bd9641fece2f920f4817", null ],
      [ "Other", "_ab_mapper_8_sample_2_models_2_person_8cs.html#aa0c8af0ac6c43d02de820d74dcf60596a6311ae17c1ee52b36e68aaf4ad066387", null ]
    ] ]
];