var _db_change_8cs =
[
    [ "DbChange", "class_ab_mapper_1_1_core_1_1_models_1_1_db_change.html", "class_ab_mapper_1_1_core_1_1_models_1_1_db_change" ],
    [ "DbChangeType", "_db_change_8cs.html#a6a6638576fd3c49eb38c2a88bd2e5c60", [
      [ "Insert", "_db_change_8cs.html#a6a6638576fd3c49eb38c2a88bd2e5c60aa458be0f08b7e4ff3c0f633c100176c0", null ],
      [ "Update", "_db_change_8cs.html#a6a6638576fd3c49eb38c2a88bd2e5c60a06933067aafd48425d67bcb01bba5cb6", null ],
      [ "Delete", "_db_change_8cs.html#a6a6638576fd3c49eb38c2a88bd2e5c60af2a6c498fb90ee345d997f888fce3b18", null ]
    ] ]
];