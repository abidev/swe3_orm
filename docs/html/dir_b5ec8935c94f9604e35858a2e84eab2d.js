var dir_b5ec8935c94f9604e35858a2e84eab2d =
[
    [ "DbChange.cs", "_db_change_8cs.html", "_db_change_8cs" ],
    [ "DbCollection.cs", "_db_collection_8cs.html", [
      [ "DbCollection", "class_ab_mapper_1_1_core_1_1_models_1_1_db_collection.html", "class_ab_mapper_1_1_core_1_1_models_1_1_db_collection" ]
    ] ],
    [ "DeleteBehaviour.cs", "_delete_behaviour_8cs.html", "_delete_behaviour_8cs" ],
    [ "OrderByDirection.cs", "_order_by_direction_8cs.html", "_order_by_direction_8cs" ],
    [ "OrderByInfo.cs", "_order_by_info_8cs.html", [
      [ "OrderByInfo", "class_ab_mapper_1_1_core_1_1_models_1_1_order_by_info.html", "class_ab_mapper_1_1_core_1_1_models_1_1_order_by_info" ]
    ] ],
    [ "RelatedCollection.cs", "_related_collection_8cs.html", [
      [ "RelatedCollection", "class_ab_mapper_1_1_core_1_1_models_1_1_related_collection.html", null ]
    ] ]
];