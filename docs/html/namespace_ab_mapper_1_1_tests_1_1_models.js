var namespace_ab_mapper_1_1_tests_1_1_models =
[
    [ "Class", "class_ab_mapper_1_1_tests_1_1_models_1_1_class.html", "class_ab_mapper_1_1_tests_1_1_models_1_1_class" ],
    [ "Course", "class_ab_mapper_1_1_tests_1_1_models_1_1_course.html", "class_ab_mapper_1_1_tests_1_1_models_1_1_course" ],
    [ "Parking", "class_ab_mapper_1_1_tests_1_1_models_1_1_parking.html", "class_ab_mapper_1_1_tests_1_1_models_1_1_parking" ],
    [ "Person", "class_ab_mapper_1_1_tests_1_1_models_1_1_person.html", "class_ab_mapper_1_1_tests_1_1_models_1_1_person" ],
    [ "Student", "class_ab_mapper_1_1_tests_1_1_models_1_1_student.html", "class_ab_mapper_1_1_tests_1_1_models_1_1_student" ],
    [ "Teacher", "class_ab_mapper_1_1_tests_1_1_models_1_1_teacher.html", "class_ab_mapper_1_1_tests_1_1_models_1_1_teacher" ]
];