var namespace_ab_mapper =
[
    [ "Core", "namespace_ab_mapper_1_1_core.html", "namespace_ab_mapper_1_1_core" ],
    [ "DataAdapters", "namespace_ab_mapper_1_1_data_adapters.html", "namespace_ab_mapper_1_1_data_adapters" ],
    [ "Sample", "namespace_ab_mapper_1_1_sample.html", "namespace_ab_mapper_1_1_sample" ],
    [ "Tests", "namespace_ab_mapper_1_1_tests.html", "namespace_ab_mapper_1_1_tests" ]
];