var indexSectionsWithContent =
{
  0: "_abcdefghijlmnopqrstuw",
  1: "acdefimnoprstu",
  2: "a",
  3: "acdefimnoprstu",
  4: "abcdefghijlmoprstuw",
  5: "_cfghilpt",
  6: "diost",
  7: "acdfimnorsu",
  8: "_abcdefghijlnopqrstw",
  9: "r"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "properties",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties",
  9: "Pages"
};

