var searchData=
[
  ['begin_138',['Begin',['../interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source.html#a23aaffa8f20009c8c9ef644a8fdee8fd',1,'AbMapper.Core.Interfaces.IDataSource.Begin()'],['../class_ab_mapper_1_1_data_adapters_1_1_postgresql_1_1_postgresql_data_source.html#a8c521dcdbe124e4ef0fc72d11d9e19b5',1,'AbMapper.DataAdapters.Postgresql.PostgresqlDataSource.Begin()']]],
  ['begintransaction_139',['BeginTransaction',['../class_ab_mapper_1_1_core_1_1_data_context.html#a19f83c804224c3e01a110418a05a1ec9',1,'AbMapper.Core.DataContext.BeginTransaction()'],['../class_ab_mapper_1_1_core_1_1_entity_cache.html#a31b55d486448ad8b43463fd00dc1e1e4',1,'AbMapper.Core.EntityCache.BeginTransaction()'],['../class_ab_mapper_1_1_core_1_1_mapper_manager.html#a798a27f34d5212c3ff8e2b005a08b0ab',1,'AbMapper.Core.MapperManager.BeginTransaction()']]],
  ['birthdate_140',['BirthDate',['../class_ab_mapper_1_1_sample_1_1_models_1_1_person.html#a56386f067f8c1d8eddbc75a45571d825',1,'AbMapper.Sample.Models.Person.BirthDate()'],['../class_ab_mapper_1_1_tests_1_1_models_1_1_person.html#ac3ecc4d0d88996434bf77e8590289ec1',1,'AbMapper.Tests.Models.Person.BirthDate()']]],
  ['buildforeignentity1toninfo_141',['BuildForeignEntity1ToNInfo',['../class_ab_mapper_1_1_core_1_1_mapper_initializer.html#a0e1ec8166e39d0c3ec9e115da6c2deb3',1,'AbMapper::Core::MapperInitializer']]],
  ['buildforeignentityntominfo_142',['BuildForeignEntityNToMInfo',['../class_ab_mapper_1_1_core_1_1_mapper_initializer.html#ab24669d91197981ff3ac5f739d141049',1,'AbMapper::Core::MapperInitializer']]]
];
