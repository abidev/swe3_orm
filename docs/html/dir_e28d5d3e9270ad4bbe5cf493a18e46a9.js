var dir_e28d5d3e9270ad4bbe5cf493a18e46a9 =
[
    [ "IDataSource.cs", "_i_data_source_8cs.html", [
      [ "IDataSource", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source.html", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source" ]
    ] ],
    [ "IDataSourceConfig.cs", "_i_data_source_config_8cs.html", [
      [ "IDataSourceConfig", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source_config.html", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_data_source_config" ]
    ] ],
    [ "IDbChange.cs", "_i_db_change_8cs.html", [
      [ "IDbChange", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_db_change.html", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_db_change" ]
    ] ],
    [ "IQueryBuilder.cs", "_i_query_builder_8cs.html", [
      [ "IQueryBuilder", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_query_builder.html", "interface_ab_mapper_1_1_core_1_1_interfaces_1_1_i_query_builder" ]
    ] ]
];