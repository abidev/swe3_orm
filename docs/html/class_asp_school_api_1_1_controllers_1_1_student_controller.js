var class_asp_school_api_1_1_controllers_1_1_student_controller =
[
    [ "StudentController", "class_asp_school_api_1_1_controllers_1_1_student_controller.html#a0e287f0cddf64d7eb96e95c3c4c7ea62", null ],
    [ "GetBirthYearStudents", "class_asp_school_api_1_1_controllers_1_1_student_controller.html#a6d4020b6227b19671d4d5b423b36d050", null ],
    [ "GetStudent", "class_asp_school_api_1_1_controllers_1_1_student_controller.html#af7dcd651de3308217bcf748aea50381e", null ],
    [ "GetStudents", "class_asp_school_api_1_1_controllers_1_1_student_controller.html#a33ec735a9ede116b55c510822132e00f", null ],
    [ "PostStudent", "class_asp_school_api_1_1_controllers_1_1_student_controller.html#a8e455b18d1fe4416b837c1f00eb49f01", null ],
    [ "RemoveStudent", "class_asp_school_api_1_1_controllers_1_1_student_controller.html#adedcb3d2fc3c2bf7341ebd3aa7aa418c", null ],
    [ "UpdateStudent", "class_asp_school_api_1_1_controllers_1_1_student_controller.html#aec889c78106317c8c60c336eafb06784", null ],
    [ "_context", "class_asp_school_api_1_1_controllers_1_1_student_controller.html#a6a312331635bc67ae1c56bdde78f2289", null ]
];