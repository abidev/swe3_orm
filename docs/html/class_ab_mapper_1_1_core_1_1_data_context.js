var class_ab_mapper_1_1_core_1_1_data_context =
[
    [ "DataContext", "class_ab_mapper_1_1_core_1_1_data_context.html#a5aced5dd301fba7585c75df27dc2d160", null ],
    [ "BeginTransaction", "class_ab_mapper_1_1_core_1_1_data_context.html#a19f83c804224c3e01a110418a05a1ec9", null ],
    [ "ClearCache", "class_ab_mapper_1_1_core_1_1_data_context.html#ad3fb1455a18ec9c479f5ba1bbbc4fde5", null ],
    [ "CommitChanges", "class_ab_mapper_1_1_core_1_1_data_context.html#aa170cecdce5c4a2a8f9f55672c8168fc", null ],
    [ "HasChanged", "class_ab_mapper_1_1_core_1_1_data_context.html#a718373a036bc5042d7821a8db2c22f33", null ],
    [ "RollbackChanges", "class_ab_mapper_1_1_core_1_1_data_context.html#af9544c20dd74120767e94dd27b0f45ab", null ],
    [ "SaveChangesAsync", "class_ab_mapper_1_1_core_1_1_data_context.html#a366a118b86ca1001c669495a11046520", null ],
    [ "Untrack", "class_ab_mapper_1_1_core_1_1_data_context.html#aaf49f1d563e1cf7617140e9cc599a2d4", null ]
];