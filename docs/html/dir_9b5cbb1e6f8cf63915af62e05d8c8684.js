var dir_9b5cbb1e6f8cf63915af62e05d8c8684 =
[
    [ "Class.cs", "_asp_school_api_2_models_2_class_8cs.html", [
      [ "Class", "class_ab_mapper_1_1_tests_1_1_models_1_1_class.html", "class_ab_mapper_1_1_tests_1_1_models_1_1_class" ]
    ] ],
    [ "Course.cs", "_asp_school_api_2_models_2_course_8cs.html", [
      [ "Course", "class_ab_mapper_1_1_tests_1_1_models_1_1_course.html", "class_ab_mapper_1_1_tests_1_1_models_1_1_course" ]
    ] ],
    [ "Person.cs", "_asp_school_api_2_models_2_person_8cs.html", "_asp_school_api_2_models_2_person_8cs" ],
    [ "Student.cs", "_asp_school_api_2_models_2_student_8cs.html", [
      [ "Student", "class_ab_mapper_1_1_tests_1_1_models_1_1_student.html", "class_ab_mapper_1_1_tests_1_1_models_1_1_student" ]
    ] ],
    [ "Teacher.cs", "_asp_school_api_2_models_2_teacher_8cs.html", [
      [ "Teacher", "class_ab_mapper_1_1_tests_1_1_models_1_1_teacher.html", "class_ab_mapper_1_1_tests_1_1_models_1_1_teacher" ]
    ] ]
];