﻿using System;
using System.Threading.Tasks;
using AbMapper.Core.Models;
using AbMapper.DataAdapters.Postgresql;
using AbMapper.Sample.Models;
using Serilog.Events;

namespace AbMapper.Sample
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Starting sample AbMapper App...");

            var now = DateTime.Now;

            var dsConfig = new PostgresqlDataSourceConfig
            {
                ConnectionString = "Host=localhost;Username=postgres;Password=password;Database=swe3",
                DropRemovedColumns = true,
                WipeDatabaseOnStartup = true,
                EnableCaching = true,
                LogLevel = LogEventLevel.Verbose
            };
            var dc = new CustomDataContext(dsConfig);

            var newTeacher = new Teacher
            {
                FirstName = "Teacher",
                LastName = "Teacherwoman",
                Sex = Sex.Female,
                HireDate = DateTime.Today
            };
            var newClass = new Class
            {
                Name = "Class1",
                Teacher = newTeacher
            };
            var newCourse = new Course { Active = true, Teacher = newTeacher, Name = "Some other Course" };
            var newStudent = new Student
            {
                FirstName = "Max",
                LastName = "Musterperson",
                BirthDate = now,
                Sex = Sex.Male,
                Courses = new RelatedCollection<Course>
                {
                    new() { Active = true, Teacher = newTeacher, Name = "Some Course" },
                    newCourse
                },
                Class = newClass
            };
            var newStudent2 = new Student
            {
                Grade = 2,
                FirstName = "Gerhard",
                LastName = "Geizig",
                BirthDate = DateTime.Now,
                Sex = Sex.Other,
                Courses = new RelatedCollection<Course> { newCourse },
                Class = newClass
            };
            
            dc.Students.Save(newStudent);
            dc.Students.Save(newStudent2);

            await dc.SaveChangesAsync();
            dc.ClearCache();
            var students = await dc.Students.Load(s => s.Courses).Load(s => s.Class).GetAllAsync();
            foreach (var student in students)
            {
                Console.WriteLine($"{student.FirstName} {student.LastName} -> {student.Sex}, {student.BirthDate}");
            }
        }
    }
}