using System.Collections.Generic;
using AbMapper.Core;
using AbMapper.Core.Interfaces;
using AbMapper.Core.Models;
using AbMapper.Sample.Models;

namespace AbMapper.Sample
{
    public class CustomDataContext: DataContext
    {
        public DbCollection<Student> Students { get; set; } = new DbCollection<Student>(); // TODO: do this automatically in mapper
        public DbCollection<Teacher> Teachers { get; set; } = new DbCollection<Teacher>();
        public DbCollection<Course> Courses { get; set; } = new DbCollection<Course>();
        public DbCollection<Class> Classes { get; set; } = new DbCollection<Class>();
        public List<string> Test { get; set; }
        public int Test2 { get; set; }
        
        public CustomDataContext(IDataSourceConfig config) : base(config)
        {
        }
    }
}