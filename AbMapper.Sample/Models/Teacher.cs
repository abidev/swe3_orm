using System;
using AbMapper.Core.Attributes;
using AbMapper.Core.Models;

namespace AbMapper.Sample.Models
{
    public class Teacher: Person
    {
        public int Salary { get; set; }
        public DateTime HireDate { get; set; }
    }
}