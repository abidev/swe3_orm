using System;
using AbMapper.Core.Attributes;

namespace AbMapper.Sample.Models
{
    public class Person
    {
        private static int _instanceId;
        
        [PrimaryKey]
        [Serial]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public Sex Sex { get; set; }

        [Excluded]
        public int InstanceId { get; } = _instanceId++;
    }

    public enum Sex
    {
        Male,
        Female,
        Other
    }
}