using System.Collections.Generic;
using AbMapper.Core.Attributes;
using AbMapper.Core.Models;

namespace AbMapper.Sample.Models
{
    public class Course
    {
        [PrimaryKey]
        [Serial]
        public int Id { get; set; }
        public bool Active { get; set; }
        public string Name { get; set; }
        
        public Teacher Teacher { get; set; }
        public RelatedCollection<Student> Students { get; set; }
    }
}