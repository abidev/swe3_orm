using System.Collections.Generic;
using AbMapper.Core.Attributes;
using AbMapper.Core.Models;

namespace AbMapper.Sample.Models
{
    public class Student: Person
    {
        public int Grade { get; set; }
        
        [DeleteBehaviour(DeleteBehaviour.Cascade)]
        public Class Class { get; set; }
        
        [DeleteBehaviour(DeleteBehaviour.Cascade)]
        public RelatedCollection<Course> Courses { get; set; }
    }
}