using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AbMapper.Core.Models;
using AbMapper.Tests.Models;
using Microsoft.AspNetCore.Mvc;

namespace AspSchoolApi.Controllers
{
    /// <summary>
    /// Defines student api endpoints.
    /// </summary>
    [Route("api/student")]
    [ApiController]
    public class StudentController: ControllerBase
    {
        private readonly SchoolDataContext _context;

        public StudentController(SchoolDataContext context)
        {
            _context = context;
        }
        
        /// <summary>
        /// Gets all students.
        /// </summary>
        /// <returns>Array with all students.</returns>
        [HttpGet]
        public async Task<IEnumerable<Student>> GetStudents()
        {
            var students = await _context.Students
                .Load(s => s.Class)
                .Load(s => s.Courses)
                .GetAllAsync();

            return students;
        }

        /// <summary>
        /// Gets all students with the given birth year.
        /// </summary>
        /// <param name="year">Birth year to search for.</param>
        /// <returns>Array with students.</returns>
        [HttpGet("birthyear/{year}")]
        public async Task<IEnumerable<Student>> GetBirthYearStudents(int year)
        {
            var start = new DateTime(year, 1, 1);
            var end = new DateTime(year, 12, 31);
            var students = await _context.Students
                .Load(s => s.Class)
                .Load(s => s.Courses)
                .Where(s => s.BirthDate >= start)
                .Where(s => s.BirthDate <= end)
                .OrderBy(s => s.BirthDate, OrderByDirection.Descending)
                .GetAllAsync();

            return students;
        }
        
        /// <summary>
        /// Gets a student.
        /// </summary>
        /// <param name="id">Student id.</param>
        /// <returns>Student.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<Student>> GetStudent(int id)
        {
            var student = await _context.Students
                .Load(s => s.Class)
                .Load(s => s.Courses)
                .GetAsync(id);

            if (student == null) return NotFound();
            
            return student;
        }
        
        /// <summary>
        /// Posts a new student.
        /// </summary>
        /// <param name="student">Student to post.</param>
        /// <returns>New student with inserted data.</returns>
        /// <remarks>
        /// Sample request:
        /// 
        ///     {
        ///         "firstName": "Max",
        ///         "lastName": "Musterperson",
        ///         "birthDate": "1997-12-28T00:00:00.000Z",
        ///         "class": {
        ///             "name": "1a",
        ///             "teacher": {
        ///                 "firstName": "Gerhard",
        ///                 "lastName": "Geizig",
        ///                 "salary": "2000"
        ///             }
        ///         },
        ///         "courses": [
        ///             {
        ///                 "active": true,
        ///                 "name": "swe3"
        ///             },
        ///             {
        ///                 "active": true,
        ///                 "name": "sks"
        ///             }
        ///         ]
        ///     }
        /// </remarks>
        [HttpPost]
        public async Task<ActionResult<Student>> PostStudent(Student student)
        {
            _context.Students.Save(student);

            await _context.SaveChangesAsync();
            
            return student;
        }
        
        /// <summary>
        /// Updates a student.
        /// </summary>
        /// <param name="student">Student to update.</param>
        /// <returns>Updated student.</returns>
        [HttpPut]
        public async Task<ActionResult<Student>> UpdateStudent(Student student)
        {
            _context.Students.Save(student);

            await _context.SaveChangesAsync();
            
            return student;
        }
        
        /// <summary>
        /// Deletes a student.
        /// </summary>
        /// <param name="id">Student id.</param>
        /// <returns>Deleted student.</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<Student>> RemoveStudent(int id)
        {
            var student = await _context.Students.GetAsync(id);

            if (student == null) return NotFound();

            _context.Students.Delete(student);

            await _context.SaveChangesAsync();
            
            return student;
        }
    }
}