using System.Collections.Generic;
using AbMapper.Core;
using AbMapper.Core.Interfaces;
using AbMapper.Core.Models;
using AbMapper.Tests.Models;

namespace AspSchoolApi
{
    /// <summary>
    /// Db context for the school api.
    /// </summary>
    public class SchoolDataContext: DataContext
    {
        public DbCollection<Student> Students { get; set; } = new DbCollection<Student>();
        public DbCollection<Teacher> Teachers { get; set; } = new DbCollection<Teacher>();
        public DbCollection<Course> Courses { get; set; } = new DbCollection<Course>();
        public DbCollection<Class> Classes { get; set; } = new DbCollection<Class>();
        
        public SchoolDataContext(IDataSourceConfig config) : base(config) { }
    }
}