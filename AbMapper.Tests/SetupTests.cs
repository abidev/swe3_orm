using System;
using System.Data.Common;
using System.Threading.Tasks;
using AbMapper.Core;
using AbMapper.DataAdapters.Postgresql;
using Npgsql;
using NUnit.Framework;

namespace AbMapper.Tests
{
    public class SetupTests
    {
        private DataContext _context;
        private DbConnection _connection;

        private const string ConnectionString = "Host=localhost;Username=postgres;Password=password;Database=swe3tests";

        [OneTimeSetUp]
        public void OnetimeSetup()
        {
            _connection = new NpgsqlConnection(ConnectionString);
            _connection.Open();
        }
        
        [SetUp]
        public void Setup()
        {
            var dsConfig = new PostgresqlDataSourceConfig
            {
                ConnectionString = ConnectionString,
                DropRemovedColumns = true,
                WipeDatabaseOnStartup = true
            };
            
            _context = new TestContext(dsConfig);
        }

        #region ORM Config

        [Test]
        public void DataContextConstructor_DoesNotThrow_WithConfig()
        {
            Assert.Pass();
        }

        #endregion

        #region Relationships
        
        [Test]
        public async Task Mapper_Builds_1To1Relationships()
        {
            await using var cmd = _connection.CreateCommand();
            await using var cmd2 = _connection.CreateCommand();
            cmd.CommandText = "SELECT count(*) FROM information_schema.columns WHERE table_schema = 'public' AND table_name = 'teacher' AND column_name = 'parkingid'";
            cmd2.CommandText = "SELECT count(*) FROM information_schema.columns WHERE table_schema = 'public' AND table_name = 'parking' AND column_name = 'teacherid'";

            var cnt = await cmd.ExecuteScalarAsync() as long?;
            var cnt2 = await cmd2.ExecuteScalarAsync() as long?;
            
            Assert.NotNull(cnt);
            Assert.NotNull(cnt2);
            Assert.Greater(cnt, 0);
            Assert.Greater(cnt2, 0);
        }

        [Test]
        public async Task Mapper_Builds_1ToNRelationships()
        {
            await using var cmd = _connection.CreateCommand();
            cmd.CommandText = "SELECT count(*) FROM information_schema.columns WHERE table_schema = 'public' AND table_name = 'class' AND column_name = 'teacherid'";

            var cnt = await cmd.ExecuteScalarAsync() as long?;
            
            Assert.NotNull(cnt);
            Assert.Greater(cnt, 0);
        }
        
        [Test]
        public async Task Mapper_Builds_NToMRelationships()
        {
            await using var cmd = _connection.CreateCommand();
            cmd.CommandText = "SELECT count(*) FROM information_schema.tables WHERE table_schema = 'public' AND (table_name = 'student_course' OR table_name = 'course_student')";

            var cnt = await cmd.ExecuteScalarAsync() as long?;
            
            Assert.NotNull(cnt);
            Assert.Greater(cnt, 0);
        }

        #endregion
    }
}