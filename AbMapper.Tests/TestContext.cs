using AbMapper.Core;
using AbMapper.Core.Interfaces;
using AbMapper.Core.Models;
using AbMapper.Tests.Models;

namespace AbMapper.Tests
{
    /// <summary>
    /// DataContext to be used in unit tests.
    /// </summary>
    public class TestContext: DataContext
    {
        public DbCollection<Student> Students { get; set; } = new DbCollection<Student>();
        public DbCollection<Teacher> Teachers { get; set; } = new DbCollection<Teacher>();
        public DbCollection<Course> Courses { get; set; } = new DbCollection<Course>();
        public DbCollection<Class> Classes { get; set; } = new DbCollection<Class>();
        public DbCollection<Parking> Parking { get; set; } = new DbCollection<Parking>();

        public TestContext(IDataSourceConfig config): base(config) { }
    }
}