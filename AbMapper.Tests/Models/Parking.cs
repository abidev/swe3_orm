using AbMapper.Core.Attributes;

namespace AbMapper.Tests.Models
{
    public class Parking
    {
        [PrimaryKey]
        [Serial]
        public int Id { get; set; }
        public string LicensePlate { get; set; }
        
        public Teacher Teacher { get; set; }
    }
}