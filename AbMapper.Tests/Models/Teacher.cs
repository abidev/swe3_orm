using System;
using AbMapper.Core.Attributes;
using AbMapper.Core.Models;

namespace AbMapper.Tests.Models
{
    public class Teacher: Person
    {
        public int Salary { get; set; }
        public DateTime HireDate { get; set; }
        
        public Parking Parking { get; set; }
        
        public RelatedCollection<Class> Classes { get; set; }
    }
}