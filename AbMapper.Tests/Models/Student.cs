using AbMapper.Core.Attributes;
using AbMapper.Core.Models;

namespace AbMapper.Tests.Models
{
    public class Student: Person
    {
        public int Grade { get; set; }
        
        [DeleteBehaviour(DeleteBehaviour.Cascade)]
        public Class Class { get; set; }
        public RelatedCollection<Course> Courses { get; set; }
    }
}