using System.Collections.Generic;
using AbMapper.Core.Attributes;
using AbMapper.Core.Models;

namespace AbMapper.Tests.Models
{
    public class Class
    {
        [PrimaryKey]
        [Serial]
        public int Id { get; set; }
        public string Name { get; set; }

        [DeleteBehaviour(DeleteBehaviour.SetNull)]
        public Teacher Teacher { get; set; }
        public RelatedCollection<Student> Students { get; set; }
    }
}