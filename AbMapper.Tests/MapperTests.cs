using System;
using System.Linq;
using System.Threading.Tasks;
using AbMapper.Core.Exceptions;
using AbMapper.Core.Models;
using AbMapper.DataAdapters.Postgresql;
using AbMapper.Tests.Models;
using Npgsql;
using NUnit.Framework;

namespace AbMapper.Tests
{
    [TestFixture]
    public class AbMapperTests
    {
        private TestContext _dataContext;
        
        [SetUp]
        public void Setup()
        {
            var dsConfig = new PostgresqlDataSourceConfig
            {
                ConnectionString = "Host=localhost;Username=postgres;Password=password;Database=swe3tests;Include Error Detail=true",
                DropRemovedColumns = true,
                WipeDatabaseOnStartup = true // Rebuilds the db after each test
            };
            _dataContext = new TestContext(dsConfig);
        }

        #region Inserts

        [Test]
        public async Task AbMapper_InsertsData_AndSetsSerialPk()
        {
            var newTeacher = new Teacher
            {
                FirstName = "Teacher",
                LastName = "Teacherwoman",
                Sex = Sex.Female
            };
            
            _dataContext.Teachers.Save(newTeacher);

            await _dataContext.SaveChangesAsync();
            
            Assert.IsNotNull(newTeacher.Id);
            Assert.AreEqual(1, newTeacher.Id);
        }
        
        [Test]
        public async Task AbMapper_InsertsData_AndInsertsRelatedData()
        {
            var newTeacher = new Teacher
            {
                FirstName = "Teacher",
                LastName = "Teacherwoman",
                Sex = Sex.Female,
                Parking = new Parking { LicensePlate = "abc" }
            };
            var newCourse = new Course
            {
                Active = true,
                Name = "SWE",
                Teacher = newTeacher
            };
            
            _dataContext.Courses.Save(newCourse);

            await _dataContext.SaveChangesAsync();
            
            Assert.IsNotNull(newTeacher.Id);
            Assert.AreEqual(1, newTeacher.Id);
        }
        
        [Test]
        public async Task AbMapper_InsertsData_AndConnectsExistingEntity()
        {
            var newTeacher = new Teacher
            {
                FirstName = "Teacher",
                LastName = "Teacherwoman",
                Sex = Sex.Female
            };
            var newCourse = new Course
            {
                Active = true,
                Name = "SWE",
                Teacher = newTeacher
            };
            
            _dataContext.Teachers.Save(newTeacher);
            await _dataContext.SaveChangesAsync();
            
            _dataContext.Courses.Save(newCourse);
            await _dataContext.SaveChangesAsync();
            
            Assert.IsNotNull(newCourse.Teacher.Id);
            Assert.AreEqual(1, newCourse.Teacher.Id);
        }

        #endregion

        #region Gets

        [Test]
        public async Task AbMapper_GetsDataEntry()
        {
            var newTeacher = new Teacher
            {
                FirstName = "Teacher",
                LastName = "Teacherwoman",
                Sex = Sex.Female
            };
            
            _dataContext.Teachers.Save(newTeacher);

            await _dataContext.SaveChangesAsync();

            var result = await _dataContext.Teachers.GetAsync(newTeacher.Id);
            
            Assert.NotNull(result);
            Assert.AreEqual(newTeacher.Id, result.Id);
        }
        
        [Test]
        public async Task AbMapper_GetsDataEntries()
        {
            var newTeacher = new Teacher
            {
                FirstName = "Teacher",
                LastName = "Teacherwoman",
                Sex = Sex.Female
            };
            var newTeacher2 = new Teacher
            {
                FirstName = "Teacher",
                LastName = "Teacherman",
                Sex = Sex.Male
            };
            
            _dataContext.Teachers.Save(newTeacher);
            _dataContext.Teachers.Save(newTeacher2);

            await _dataContext.SaveChangesAsync();

            var result = await _dataContext.Teachers.GetAllAsync();
            
            Assert.NotNull(result);
            Assert.AreEqual(2, result.Count());
        }

        [Test]
        public async Task AbMapper_GetsDataEntry_AndDoesNotEagerLoad()
        {
            var newStudent = new Student
            {
                FirstName = "Max",
                LastName = "Musterperson",
                BirthDate = DateTime.Now,
                Sex = Sex.Male,
                Courses = new RelatedCollection<Course>
                {
                    new() { Active = true, Name = "Some Course" }
                },
                Class = new Class {
                    Name = "Class1"
                }
            };
            
            _dataContext.Students.Save(newStudent);

            await _dataContext.SaveChangesAsync();
            _dataContext.Untrack(newStudent);

            var result = await _dataContext.Students.GetAsync(newStudent.Id);
            
            Assert.NotNull(result);
            Assert.AreEqual(newStudent.Id, result.Id);
            
            Assert.Null(result.Class);
        }
        
        [Test]
        public async Task AbMapper_GetsDataEntry_AndEagerLoads1ToNOneSide()
        {
            var newClass = new Class
            {
                Name = "Class1"
            };
            var newStudent = new Student
            {
                FirstName = "Max",
                LastName = "Musterperson",
                BirthDate = DateTime.Now,
                Sex = Sex.Male,
                Courses = new RelatedCollection<Course>
                {
                    new() { Active = true, Name = "Some Course" }
                },
                Class = newClass
            };
            
            _dataContext.Students.Save(newStudent);

            await _dataContext.SaveChangesAsync();

            var result = await _dataContext.Students.Load(s => s.Class).GetAsync(newStudent.Id);
            
            Assert.NotNull(result);
            Assert.AreEqual(newStudent.Id, result.Id);
            
            Assert.NotNull(result.Class);
            Assert.AreEqual(newClass.Name, result.Class.Name);
        }
        
        [Test]
        public async Task AbMapper_GetsDataEntry_AndEagerLoads1ToNOneSide_NullWhenEmpty()
        {
            var newStudent = new Student
            {
                FirstName = "Max",
                LastName = "Musterperson",
                BirthDate = DateTime.Now,
                Sex = Sex.Male,
                Courses = new RelatedCollection<Course>
                {
                    new() { Active = true, Name = "Some Course" }
                }
            };
            
            _dataContext.Students.Save(newStudent);

            await _dataContext.SaveChangesAsync();

            var result = await _dataContext.Students.Load(s => s.Class).GetAsync(newStudent.Id);
            
            Assert.NotNull(result);
            Assert.AreEqual(newStudent.Id, result.Id);
            
            Assert.Null(result.Class);
        }
        
        [Test]
        public async Task AbMapper_GetsDataEntry_AndEagerLoads1ToNNSide()
        {
            var newClass = new Class
            {
                Name = "Class1"
            };
            var newStudent = new Student
            {
                FirstName = "Max",
                LastName = "Musterperson",
                BirthDate = DateTime.Now,
                Sex = Sex.Male,
                Courses = new RelatedCollection<Course>
                {
                    new() { Active = true, Name = "Some Course" }
                },
                Class = newClass
            };
            var newStudent2 = new Student
            {
                FirstName = "Gerhard",
                LastName = "Geizig",
                BirthDate = DateTime.Now,
                Sex = Sex.Other,
                Class = newClass
            };
            
            _dataContext.Students.Save(newStudent);
            _dataContext.Students.Save(newStudent2);

            await _dataContext.SaveChangesAsync();

            var result = await _dataContext.Classes.Load(c => c.Students).GetAsync(newClass.Id);
            
            Assert.NotNull(result);
            Assert.AreEqual(newClass.Id, result.Id);
            
            Assert.NotNull(result.Students);
            Assert.AreEqual(2, result.Students.Count);
        }
        
        [Test]
        public async Task AbMapper_GetsDataEntry_AndEagerLoadsNToM()
        {
            var newStudent = new Student
            {
                FirstName = "Max",
                LastName = "Musterperson",
                BirthDate = DateTime.Now,
                Sex = Sex.Male,
                Courses = new RelatedCollection<Course>
                {
                    new() { Active = true, Name = "Some Course" },
                    new () { Active = true, Name = "Some other Course" }
                }
            };
            
            _dataContext.Students.Save(newStudent);

            await _dataContext.SaveChangesAsync();

            var result = await _dataContext.Students.Load(s => s.Courses).GetAsync(newStudent.Id);
            
            Assert.NotNull(result);
            Assert.AreEqual(newStudent.Id, result.Id);
            
            Assert.NotNull(result.Courses);
            Assert.AreEqual(2, result.Courses.Count);
        }
        
        [Test]
        public async Task AbMapper_GetsDataEntry_AndEagerLoadsNToM_WithEmptyList()
        {
            var newStudent = new Student
            {
                FirstName = "Max",
                LastName = "Musterperson",
                BirthDate = DateTime.Now,
                Sex = Sex.Male
            };
            
            _dataContext.Students.Save(newStudent);

            await _dataContext.SaveChangesAsync();

            var result = await _dataContext.Students.Load(s => s.Courses).GetAsync(newStudent.Id);
            
            Assert.NotNull(result);
            Assert.AreEqual(newStudent.Id, result.Id);
            
            Assert.NotNull(result.Courses);
            Assert.AreEqual(0, result.Courses.Count);
        }
        
        [Test]
        public async Task AbMapper_GetsDataEntries_Limited()
        {
            var newTeacher = new Teacher
            {
                FirstName = "Teacher",
                LastName = "Teacherwoman",
                Sex = Sex.Female
            };
            var newTeacher2 = new Teacher
            {
                FirstName = "Teacher",
                LastName = "Teacherman",
                Sex = Sex.Male
            };
            
            _dataContext.Teachers.Save(newTeacher);
            _dataContext.Teachers.Save(newTeacher2);

            await _dataContext.SaveChangesAsync();

            var result = await _dataContext.Teachers.Limit(1).GetAllAsync();
            
            Assert.NotNull(result);
            Assert.AreEqual(1, result.Count());
        }
        
        [Test]
        public async Task AbMapper_GetsDataEntries_Skipping()
        {
            var newTeacher = new Teacher
            {
                FirstName = "Teacher",
                LastName = "Teacherwoman",
                Sex = Sex.Female
            };
            var newTeacher2 = new Teacher
            {
                FirstName = "Teacher",
                LastName = "Teacherman",
                Sex = Sex.Male
            };
            
            _dataContext.Teachers.Save(newTeacher);
            _dataContext.Teachers.Save(newTeacher2);

            await _dataContext.SaveChangesAsync();

            var result = await _dataContext.Teachers.Skip(1).GetAllAsync();
            
            Assert.NotNull(result);
            Assert.AreEqual(1, result.Count());
            Assert.AreEqual(newTeacher2.Id, result.First().Id);
        }
        
        [Test]
        public async Task AbMapper_GetsDataEntries_OrdersAscendingByDefault()
        {
            var newTeacher = new Teacher
            {
                FirstName = "Teacher",
                LastName = "Teacherwoman",
                Sex = Sex.Female
            };
            var newTeacher2 = new Teacher
            {
                FirstName = "Teacher",
                LastName = "Teacherman",
                Sex = Sex.Male
            };
            
            _dataContext.Teachers.Save(newTeacher);
            _dataContext.Teachers.Save(newTeacher2);

            await _dataContext.SaveChangesAsync();

            var result = await _dataContext.Teachers.OrderBy(t => t.Sex).GetAllAsync();
            
            Assert.NotNull(result);
            Assert.AreEqual(newTeacher2.Id, result.First().Id);
        }
        
        [Test]
        public async Task AbMapper_GetsDataEntries_OrderDescending()
        {
            var newTeacher = new Teacher
            {
                FirstName = "Teacher",
                LastName = "Teacherwoman",
                Sex = Sex.Female
            };
            var newTeacher2 = new Teacher
            {
                FirstName = "Teacher",
                LastName = "Teacherman",
                Sex = Sex.Male
            };
            
            _dataContext.Teachers.Save(newTeacher);
            _dataContext.Teachers.Save(newTeacher2);

            await _dataContext.SaveChangesAsync();

            var result = await _dataContext.Teachers.OrderBy(t => t.Sex, OrderByDirection.Descending).GetAllAsync();
            
            Assert.NotNull(result);
            Assert.AreEqual(newTeacher.Id, result.First().Id);
        }

        #endregion

        #region Where

        [Test]
        public async Task AbMapper_GetsData_LimitedByWhereExpression()
        {
            var newStudent = new Student
            {
                FirstName = "Max",
                LastName = "Musterperson",
                BirthDate = DateTime.Now,
                Sex = Sex.Male,
                Courses = new RelatedCollection<Course>
                {
                    new() { Active = true, Name = "Some Course" }
                }
            };
            var newStudent2 = new Student
            {
                FirstName = "Gerhard",
                LastName = "Geizig",
                BirthDate = DateTime.Now,
                Sex = Sex.Other
            };
            
            _dataContext.Students.Save(newStudent);
            _dataContext.Students.Save(newStudent2);

            await _dataContext.SaveChangesAsync();

            var result = await _dataContext.Students.GetAllAsync();
            
            Assert.NotNull(result);
            Assert.AreEqual(2, result.Count());
            
            
            var result2 = await _dataContext.Students.Where(s => s.FirstName == "Max").GetAllAsync();
            
            Assert.NotNull(result2);
            Assert.AreEqual(1, result2.Count());
        }
        
        [Test]
        public async Task AbMapper_GetsData_LimitedByWhereExpression_EnumProperty()
        {
            var newStudent = new Student
            {
                FirstName = "Max",
                LastName = "Musterperson",
                BirthDate = DateTime.Now,
                Sex = Sex.Male,
                Courses = new RelatedCollection<Course>
                {
                    new() { Active = true, Name = "Some Course" }
                }
            };
            var newStudent2 = new Student
            {
                FirstName = "Gerhard",
                LastName = "Geizig",
                BirthDate = DateTime.Now,
                Sex = Sex.Other
            };
            
            _dataContext.Students.Save(newStudent);
            _dataContext.Students.Save(newStudent2);

            await _dataContext.SaveChangesAsync();

            var result = await _dataContext.Students.Where(s => s.Sex == Sex.Other).GetAllAsync();
            
            Assert.NotNull(result);
            Assert.AreEqual(1, result.Count());
        }
        
        [Test]
        public async Task AbMapper_GetsData_LimitedByWhereExpression_NonConstantProperty()
        {
            var now = DateTime.Now;
            
            var newStudent = new Student
            {
                BirthDate = now,
                FirstName = "Max",
                LastName = "Musterperson",
                Sex = Sex.Male,
                Courses = new RelatedCollection<Course>
                {
                    new() { Active = true, Name = "Some Course" }
                }
            };
            var newStudent2 = new Student
            {
                FirstName = "Gerhard",
                LastName = "Geizig",
                BirthDate = DateTime.Now,
                Sex = Sex.Other
            };
            
            _dataContext.Students.Save(newStudent);
            _dataContext.Students.Save(newStudent2);

            await _dataContext.SaveChangesAsync();

            var result = await _dataContext.Students.Where(s => s.BirthDate == now).GetAllAsync();
            
            Assert.NotNull(result);
            Assert.AreEqual(1, result.Count());
        }

        #endregion

        #region Cache

        [Test]
        public async Task AbMapper_CachesData_OnInsert()
        {
            var newStudent = new Student
            {
                BirthDate = DateTime.Now,
                FirstName = "Max",
                LastName = "Musterperson",
                Sex = Sex.Male,
                Courses = new RelatedCollection<Course>
                {
                    new() { Active = true, Name = "Some Course" }
                }
            };
            
            _dataContext.Students.Save(newStudent);
            
            await _dataContext.SaveChangesAsync();
            
            var result = await _dataContext.Students.Load(s => s.Courses).GetAsync(newStudent.Id);
            
            Assert.AreEqual(newStudent.InstanceId, result.InstanceId);
        }
        
        [Test]
        public async Task AbMapper_CachesData_OnGet()
        {
            var newStudent = new Student
            {
                BirthDate = DateTime.Now,
                FirstName = "Max",
                LastName = "Musterperson",
                Sex = Sex.Male,
                Courses = new RelatedCollection<Course>
                {
                    new() { Active = true, Name = "Some Course" }
                }
            };
            
            _dataContext.Students.Save(newStudent);
            
            await _dataContext.SaveChangesAsync();
            
            var result1 = await _dataContext.Students.GetAsync(newStudent.Id);
            var result2 = await _dataContext.Students.GetAsync(newStudent.Id);
            
            Assert.AreEqual(result1.InstanceId, result2.InstanceId);
        }
        
        [Test]
        public async Task AbMapper_DoesNotUseCachedData_OnGet_WithDifferentRequestedRelatedData()
        {
            var newStudent = new Student
            {
                BirthDate = DateTime.Now,
                FirstName = "Max",
                LastName = "Musterperson",
                Sex = Sex.Male,
                Courses = new RelatedCollection<Course>
                {
                    new() { Active = true, Name = "Some Course" }
                }
            };
            
            _dataContext.Students.Save(newStudent);
            
            await _dataContext.SaveChangesAsync();
            
            var result1 = await _dataContext.Students.GetAsync(newStudent.Id);
            var result2 = await _dataContext.Students.Load(s => s.Class).GetAsync(newStudent.Id);
            
            Assert.AreNotEqual(result1.InstanceId, result2.InstanceId);
        }

        #endregion

        #region Delete

        [Test]
        public async Task AbMapper_DeleteData_FailsWithRelatedEntries()
        { 
            var newStudent = new Student
            {
                BirthDate = DateTime.Now,
                FirstName = "Max",
                LastName = "Musterperson",
                Sex = Sex.Male,
                Courses = new RelatedCollection<Course> // Has DeleteBehaviour.NoAction (Default)
                {
                    new() { Active = true, Name = "Some Course" }
                }
            };
            var newStudent2 = new Student
            {
                FirstName = "Gerhard",
                LastName = "Geizig",
                BirthDate = DateTime.Now,
                Sex = Sex.Other
            };
            
            _dataContext.Students.Save(newStudent);
            _dataContext.Students.Save(newStudent2);

            await _dataContext.SaveChangesAsync();
            
            _dataContext.Students.Delete(newStudent);

            Assert.ThrowsAsync<PostgresException>(async () =>
            {
                await _dataContext.SaveChangesAsync();
            });
        }
        
        [Test]
        public async Task AbMapper_DeleteData_CascadesDelete()
        {
            var newClass = new Class
            {
                Name = "Class1"
            };
            var newStudent = new Student
            {
                BirthDate = DateTime.Now,
                FirstName = "Max",
                LastName = "Musterperson",
                Sex = Sex.Male,
                Class = newClass // Has DeleteBehaviour.Cascade
            };
            
            _dataContext.Students.Save(newStudent);

            await _dataContext.SaveChangesAsync();
            
            _dataContext.Classes.Delete(newClass);

            await _dataContext.SaveChangesAsync();

            var result = await _dataContext.Students.GetAllAsync();
            
            Assert.NotNull(result);
            Assert.AreEqual(0, result.Count());
        }
        
        [Test]
        public async Task AbMapper_DeleteData_NullsRelatedEntryFks()
        {
            var newTeacher = new Teacher
            {
                FirstName = "Teacher",
                LastName = "Teacherwoman",
                Sex = Sex.Female
            };
            
            var newClass = new Class
            {
                Name = "Class1",
                Teacher = newTeacher // Has DeleteBehaviour.SetNull
            };

            _dataContext.Classes.Save(newClass);

            await _dataContext.SaveChangesAsync();
            
            _dataContext.Teachers.Delete(newTeacher);

            await _dataContext.SaveChangesAsync();

            var result = await _dataContext.Classes.Load(c => c.Teacher).GetAsync(newClass.Id);
            
            Assert.NotNull(result);
            Assert.Null(result.Teacher);
        }

        #endregion

        #region Update

        [Test]
        public async Task AbMapper_UpdatesData()
        {
            var newTeacher = new Teacher
            {
                FirstName = "Teacher",
                LastName = "Teacherwoman",
                Sex = Sex.Female
            };
            
            _dataContext.Teachers.Save(newTeacher);

            await _dataContext.SaveChangesAsync();
            var updatedName = "NewTeachername";

            newTeacher.FirstName = updatedName;

            await _dataContext.SaveChangesAsync();
            _dataContext.ClearCache();

            var foundTeacher = await _dataContext.Teachers.GetAsync(newTeacher.Id);
            
            Assert.AreEqual(updatedName, foundTeacher.FirstName);
            // Ensure that found teacher is not cached, therefore db was updated correctly
            Assert.AreNotEqual(newTeacher.InstanceId, foundTeacher.InstanceId);
        }
        
        [Test]
        public async Task AbMapper_UpdatesData_AndUpdatesRelatedData()
        {
            var newTeacher = new Teacher
            {
                FirstName = "Teacher",
                LastName = "Teacherwoman",
                Sex = Sex.Female,
                Parking = new Parking { LicensePlate = "abc" }
            };
            var newCourse = new Course
            {
                Active = true,
                Name = "SWE",
                Teacher = newTeacher
            };
            
            _dataContext.Courses.Save(newCourse);

            await _dataContext.SaveChangesAsync();

            newCourse.Teacher = new Teacher { FirstName = "newTeacher", LastName = "LastName", Sex = Sex.Other };

            await _dataContext.SaveChangesAsync();
            _dataContext.ClearCache();
            
            var foundCourse = await _dataContext.Courses.Load(c => c.Teacher).GetAsync(newCourse.Id);
            Assert.AreNotEqual(newTeacher.Id, foundCourse.Teacher.Id);
        }
        
        [Test]
        public async Task AbMapper_UpdatesData_AndIgnoresRelatedNToMData()
        {
            var newCourses = new RelatedCollection<Course>
            {
                new() {Active = true, Name = "Some Course"},
                new() {Active = true, Name = "Some other Course"}
            };
            var newStudent = new Student
            {
                FirstName = "Max",
                LastName = "Musterperson",
                BirthDate = DateTime.Now,
                Sex = Sex.Male,
                Courses = newCourses
            };
            
            _dataContext.Students.Save(newStudent);

            await _dataContext.SaveChangesAsync();

            newStudent.Courses = new RelatedCollection<Course> {new () {Active = true, Name = "Lonely course"}};

            await _dataContext.SaveChangesAsync();
            _dataContext.ClearCache();
            
            var foundCourse = await _dataContext.Students.Load(s => s.Courses).GetAsync(newStudent.Id);
            Assert.AreEqual(newCourses.Count, foundCourse.Courses.Count);
        }

        #endregion

        #region Change Tracking
        
        [Test]
        public async Task AbMapper_TracksChanges_OnUpdate()
        {
            var newStudent = new Student
            {
                FirstName = "Max",
                LastName = "Musterperson",
                BirthDate = DateTime.Now,
                Sex = Sex.Male
            };
            
            _dataContext.Students.Save(newStudent);

            await _dataContext.SaveChangesAsync();
            
            Assert.AreEqual(false, _dataContext.HasChanged(newStudent));

            newStudent.FirstName = "newFirstName";
            
            Assert.AreEqual(true, _dataContext.HasChanged(newStudent));
        }
        
        [Test]
        public async Task AbMapper_TracksChanges_Throws_WhenTryingToTrackEntryMultipleTimes()
        {
            var newStudent = new Student
            {
                FirstName = "Max",
                LastName = "Musterperson",
                BirthDate = DateTime.Now,
                Sex = Sex.Male,
                Courses = new RelatedCollection<Course>
                {
                    new() { Active = true, Name = "Some Course" }
                },
                Class = new Class {
                    Name = "Class1"
                }
            };
            
            _dataContext.Students.Save(newStudent);

            await _dataContext.SaveChangesAsync();
            _dataContext.Untrack(newStudent);

            await _dataContext.Students.GetAsync(newStudent.Id);

            Assert.ThrowsAsync<AbTrackingException>(async () =>
                await _dataContext.Students.Load(s => s.Class).GetAsync(newStudent.Id));
        }
        
        [Test]
        public async Task AbMapper_TracksChanges_AllowsTrackingAfterUntrackingOtherInstance()
        {
            var newStudent = new Student
            {
                FirstName = "Max",
                LastName = "Musterperson",
                BirthDate = DateTime.Now,
                Sex = Sex.Male,
                Courses = new RelatedCollection<Course>
                {
                    new() { Active = true, Name = "Some Course" }
                },
                Class = new Class {
                    Name = "Class1"
                }
            };
            
            _dataContext.Students.Save(newStudent);

            await _dataContext.SaveChangesAsync();
            _dataContext.Untrack(newStudent);

            var result = await _dataContext.Students.GetAsync(newStudent.Id);
            _dataContext.Untrack(result);

            Assert.DoesNotThrowAsync(async () =>
                await _dataContext.Students.Load(s => s.Class).GetAsync(newStudent.Id));
        }

        #endregion

        #region Transactions

        [Test]
        public async Task AbMapper_Transaction_CommitsChanges()
        {
            var newTeacher = new Teacher
            {
                FirstName = "Teacher",
                LastName = "Teacherwoman",
                Sex = Sex.Female
            };
            
            _dataContext.Teachers.Save(newTeacher);

            await _dataContext.SaveChangesAsync();
            
            await _dataContext.BeginTransaction();
            
            var updatedName = "NewTeachername";
            newTeacher.FirstName = updatedName;
            await _dataContext.SaveChangesAsync();
            
            await _dataContext.CommitChanges();

            var foundTeacher = await _dataContext.Teachers.GetAsync(newTeacher.Id);
            
            Assert.AreEqual(updatedName, foundTeacher.FirstName);
        }
        
        [Test]
        public async Task AbMapper_Transaction_RollbacksChanges()
        {
            var newTeacher = new Teacher
            {
                FirstName = "Teacher",
                LastName = "Teacherwoman",
                Sex = Sex.Female
            };
            
            _dataContext.Teachers.Save(newTeacher);

            await _dataContext.SaveChangesAsync();
            
            await _dataContext.BeginTransaction();
            
            var updatedName = "NewTeachername";
            newTeacher.FirstName = updatedName;
            await _dataContext.SaveChangesAsync();
            
            await _dataContext.RollbackChanges();

            var foundTeacher = await _dataContext.Teachers.GetAsync(newTeacher.Id);
            
            Assert.AreNotEqual(updatedName, foundTeacher.FirstName);
        }

        #endregion
    }
}