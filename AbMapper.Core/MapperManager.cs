using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using AbMapper.Core.Exceptions;
using AbMapper.Core.Models;
using AbMapper.Core.Reflection;
using AbMapper.Core.Interfaces;
using Serilog;

namespace AbMapper.Core
{
    /// <summary>
    /// Manages the active datasource and datacontext and acts as a proxy between them.
    /// </summary>
    internal class MapperManager
    {
        private static readonly Lazy<MapperManager> LazyInstance = new Lazy<MapperManager>(() => new MapperManager());
        public static MapperManager Instance => LazyInstance.Value;
        
        /// <summary>
        /// Currently active datasource.
        /// </summary>
        private IDataSource ActiveDataSource { get; set; }
        
        /// <summary>
        /// Config of currently active datasource.
        /// </summary>
        private IDataSourceConfig ActiveDataSourceConfig { get; set; }
        
        /// <summary>
        /// Currently active datacontext.
        /// </summary>
        private DataContext ActiveDataContext { get; set; }
        
        /// <summary>
        /// List of discovered entities with analyzed metadata.
        /// </summary>
        private Dictionary<Type, Entity> Entities { get; set; }

        /// <summary>
        /// Holds entity caches for registered entities. Entity caches are added lazily.
        /// </summary>
        private Dictionary<Type, EntityCache> EntityCaches { get; set; } = new Dictionary<Type, EntityCache>();
        
        /// <summary>
        /// List of changes to be persisted to datasource by calling SaveChanges.
        /// </summary>
        private List<IDbChange> _dbChanges = new List<IDbChange>();

        /// <summary>
        /// Holds the mappers current transaction state.
        /// </summary>
        private TransactionState _transactionState = TransactionState.None;

        /// <summary>
        /// Sets manager initial data from mapper initializer results.
        /// </summary>
        /// <param name="dataSource"></param>
        /// <param name="config"></param>
        /// <param name="context"></param>
        /// <param name="entities"></param>
        public void UseInitializationResults(IDataSource dataSource, IDataSourceConfig config, DataContext context, Dictionary<Type, Entity> entities)
        {
            ActiveDataSource = dataSource;
            ActiveDataSourceConfig = config;
            ActiveDataContext = context;
            Entities = entities;
        }

        #region Data Manipulation

        /// <summary>
        /// Returns an the instance of this entity specified by the given primary keys.
        /// </summary>
        /// <param name="includedNavigationMembers"></param>
        /// <param name="whereExpressions"></param>
        /// <param name="pks"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns>
        ///     Instance: entry found
        ///     Null: entry not found
        /// </returns>
        /// <exception cref="AbMapperException">Thrown if no entity with the given type is registered with AbMapper</exception>
        public async Task<T> GetAsync<T>(IEnumerable<MemberInfo> includedNavigationMembers, IEnumerable<BinaryExpression> whereExpressions, params object[] pks) where T : class, new()
        {
            var entity = Util.FindEntityByType(typeof(T), Entities);

            var cacheEntry = GetCacheEntry(entity, pks);

            var navEntityInfos = includedNavigationMembers.Select(nm =>
                entity.ForeignEntityInfos.FirstOrDefault(fe => fe.ForeignEntityProperty.Name == nm.Name)).ToList();

            if (cacheEntry != null && UseCachedEntry(entity, navEntityInfos, cacheEntry))
            {
                Log.Logger.Debug("{Class}: Returning cache hit for {Entity}", nameof(MapperManager), entity.Type.Name);
                return cacheEntry as T;
            }
            
            var result = await ActiveDataSource.GetAsync<T>(entity, navEntityInfos, whereExpressions, pks);
            InsertCacheEntry(entity, result);
            Log.Logger.Information("{Class}: Loaded entry for entity {Entity}", nameof(MapperManager), entity.Type.Name);
            return result;
        }
        
        /// <summary>
        /// Returns an enumerable of all instances of this entity.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <exception cref="AbMapperException">Thrown if no entity with the given type is registered with AbMapper</exception>
        public async Task<IEnumerable<T>> GetAllAsync<T>(IEnumerable<MemberInfo> includedNavigationMembers, IEnumerable<BinaryExpression> whereExpressions, 
            bool eagerLoad = false, int? limit = null, int? skip = null, OrderByInfo orderByInfo = null) where T: class, new()
        {
            var entity = Util.FindEntityByType(typeof(T), Entities);

            var navEntityInfos = includedNavigationMembers.Select(nm =>
                entity.ForeignEntityInfos.FirstOrDefault(fe => fe.ForeignEntityProperty.Name == nm.Name));
            
            var result = await ActiveDataSource.GetAllAsync<T>(entity, eagerLoad, navEntityInfos, whereExpressions, 
                limit, skip, orderByInfo);
            InsertCacheEntries(entity, result);
            Log.Logger.Information("{Class}: Loaded entries for entity {Entity}", nameof(MapperManager), entity.Type.Name);
            return result;
        }

        /// <summary>
        /// Registers an insert DbChange.
        /// </summary>
        /// <param name="data">Data to be inserted</param>
        /// <typeparam name="T"></typeparam>
        /// <exception cref="AbMapperException">Thrown if no entity with the given type is registered with AbMapper</exception>
        public void InsertData<T>(T data)
        {
            var entity = Util.FindEntityByType(data.GetType(), Entities);

            var change = new DbChange(DbChangeType.Insert, data);
            _dbChanges.Add(change);
            Log.Logger.Information("{Class}: Registered new entry for entity {Entity}", nameof(MapperManager), entity.Type.Name);
        }

        /// <summary>
        /// Registers a delete DbChange.
        /// </summary>
        /// <param name="data">Data to be deleted</param>
        /// <typeparam name="T"></typeparam>
        public void DeleteData<T>(T data)
        {
            var entity = Util.FindEntityByType(data.GetType(), Entities);

            var change = new DbChange(DbChangeType.Delete, data);
            _dbChanges.Add(change);
            Log.Logger.Information("{Class}: Registered entry deletion for entity {Entity}", nameof(MapperManager), entity.Type.Name);
        }

        /// <summary>
        /// Persists all registered changes to the configured datasource.
        /// </summary>
        /// <exception cref="AbMapperException">Thrown if no entity is registered for a type of a DbChange</exception>
        public async Task SaveChangesAsync()
        {
            CheckCachesForUpdates();
            Log.Logger.Debug("{Class}: Registered updates from cache changes", nameof(MapperManager));
            
            foreach (var change in _dbChanges)
            {
                var entity = Util.FindEntityByType(change.Type, Entities);
                if (entity == null) throw new AbMapperException("No entity registered for this type");
                
                switch (change.ChangeType)
                {
                    case DbChangeType.Insert:
                    case DbChangeType.Update:
                        
                        await ActiveDataSource.InsertAsync(entity, change.Data);
                        InsertCacheEntry(entity, change.Data);
                        
                        break;
                    case DbChangeType.Delete:

                        await ActiveDataSource.RemoveAsync(entity, change.Data);
                        RemoveCacheEntry(entity, change.Data);
                        
                        // Drop caches for each foreign type to avoid inconsistent cache content
                        foreach (var feInfo in entity.ForeignEntityInfos)
                        {
                            if (feInfo.ForeignEntityProperty?.DeleteBehaviour == DeleteBehaviour.Cascade || 
                                feInfo.ForeignEntityProperty?.DeleteBehaviour == DeleteBehaviour.SetNull)
                                EntityCaches.Remove(feInfo.ForeignEntity.Type);
                        }
                        
                        break;
                }
            }
            
            _dbChanges = new List<IDbChange>();
            Log.Logger.Information("{Class}: Saved registered changes to data source", nameof(MapperManager));
        }

        #endregion

        #region Cache

        /// <summary>
        /// Finds or create the cache for the given entity and gets cache entry for given pks.
        /// </summary>
        /// <param name="entity">Entity cache to use.</param>
        /// <param name="pks">Pks to search for.</param>
        /// <returns>Cache entry of null if not found.</returns>
        private object GetCacheEntry(Entity entity, params object[] pks)
        {
            if (!ActiveDataSourceConfig.EnableCaching) return null;
            
            if (EntityCaches.ContainsKey(entity.Type) && _transactionState == TransactionState.None) 
                return EntityCaches[entity.Type].Get(pks: pks);
            if (EntityCaches.ContainsKey(entity.Type))
                return EntityCaches[entity.Type].GetTransactional(pks);
            
            EntityCaches.Add(entity.Type, new EntityCache(entity));
            return null;
        }

        /// <summary>
        /// Inserts an entry into the cache for the given entity or creates a new cache.
        /// </summary>
        /// <param name="entity">Entity cache to use.</param>
        /// <param name="entry">Entry to insert into cache.</param>
        private void InsertCacheEntry(Entity entity, object entry)
        {
            if (!ActiveDataSourceConfig.EnableCaching) return;
            
            if (!EntityCaches.ContainsKey(entity.Type)) 
                EntityCaches.Add(entity.Type, new EntityCache(entity));
            
            if (_transactionState == TransactionState.None)
                EntityCaches[entity.Type].Insert(entry);
            else
                EntityCaches[entity.Type].InsertTransactional(entry);
        }

        /// <summary>
        /// Removes an entry from the cache for the given entity.
        /// </summary>
        /// <param name="entity">Entity cache to use.</param>
        /// <param name="entry">Entry to delete from the cache.</param>
        private void RemoveCacheEntry(Entity entity, object entry)
        {
            if (!ActiveDataSourceConfig.EnableCaching) return;
            
            if (EntityCaches.ContainsKey(entity.Type) && _transactionState == TransactionState.None) 
                EntityCaches[entity.Type].Remove(entry);
            else if (EntityCaches.ContainsKey(entity.Type))
                EntityCaches[entity.Type].RemoveTransactional(entry);
        }

        /// <summary>
        /// Inserts many entries into the cache for the given entity or creates a new cache.
        /// </summary>
        /// <param name="entity">Entity cache to use.</param>
        /// <param name="entries">Entries to insert into cache.</param>
        private void InsertCacheEntries(Entity entity, IEnumerable<object> entries)
        {
            if (!ActiveDataSourceConfig.EnableCaching) return;

            foreach (var entry in entries)
            {
                InsertCacheEntry(entity, entry);
            }
        }
        
        /// <summary>
        /// Checks if the given entry has changed. Changes within transactions are not considered.
        /// </summary>
        /// <param name="entry">Entry to check for changes.</param>
        /// <returns>True if entry has changed, false otherwise.</returns>
        public bool HasEntryChanged(object entry)
        {
            var entity = Util.FindEntityByType(entry.GetType(), Entities);

            return EntityCaches.ContainsKey(entity.Type) && EntityCaches[entity.Type].HasChanged(entry);
        }
        
        /// <summary>
        /// Untracks the given entry. This deletes the entry from cache and allows a new entry to be tracked.
        /// </summary>
        /// <param name="entry">Entry to untrack.</param>
        public void UntrackEntry(object entry)
        {
            var entity = Util.FindEntityByType(entry.GetType(), Entities);

            if (EntityCaches.ContainsKey(entity.Type) && _transactionState == TransactionState.None)
                EntityCaches[entity.Type].Remove(entry);
            else if (EntityCaches.ContainsKey(entity.Type))
                EntityCaches[entity.Type].RemoveTransactional(entry);
        }
        
        /// <summary>
        /// Checks all registered caches for updates and registers db changes.
        /// </summary>
        private void CheckCachesForUpdates()
        {
            foreach (var (_, cache) in EntityCaches)
            {
                var updatedEntries = cache.GetUpdatedEntries();
                foreach (var entry in updatedEntries)
                {
                    _dbChanges.Add(new DbChange(DbChangeType.Update, entry));
                }
            }
        }

        /// <summary>
        /// Deletes caches for all entities.
        /// </summary>
        public void ClearCaches()
        {
            EntityCaches = new Dictionary<Type, EntityCache>();
        }
        
        /// <summary>
        /// Checks if the given cache object should be used or thrown out based on eager loaded related data.
        /// </summary>
        /// <param name="entity">Entity for given object.</param>
        /// <param name="feInfos">List of requested eager loaded data.</param>
        /// <param name="data">Cached object.</param>
        /// <returns>True if cache object should be used, false if cache object should be thrown out.</returns>
        private static bool UseCachedEntry(Entity entity, IEnumerable<ForeignEntityInfo> feInfos, object data)
        {
            var allFeInfos = entity.ForeignEntityInfos.ToList();
            foreach (var feInfo in feInfos) // Fe infos that should be included, return false if null
            {
                var value = feInfo?.ForeignEntityProperty.PropertyInfo.GetValue(data);
                allFeInfos.Remove(feInfo);
                if (value == null) return false;
            }

            foreach (var feInfo in allFeInfos) // Fe infos that should not be included, return false if set
            {
                var value = feInfo?.ForeignEntityProperty.PropertyInfo.GetValue(data);
                if (value != null) return false;
            }

            return true;
        }

        #endregion

        #region Transactions

        /// <summary>
        /// Starts a new transaction containing the following insert, update or delete statements.
        /// </summary>
        public async Task BeginTransaction()
        {
            _transactionState = TransactionState.Open;
            await ActiveDataSource.Begin();
            foreach (var (_, cache) in EntityCaches)
            {
                cache.BeginTransaction();
            }
            Log.Logger.Information("{Class}: Started transaction", nameof(MapperManager));
        }

        /// <summary>
        /// Commits the transactions changes.
        /// </summary>
        public async Task CommitChanges()
        {
            _transactionState = TransactionState.None;
            await ActiveDataSource.Commit();
            foreach (var (_, cache) in EntityCaches)
            {
                cache.CommitTransaction();
            }
            Log.Logger.Information("{Class}: Committed transaction", nameof(MapperManager));
        }

        /// <summary>
        /// Rollbacks the transactions changes.
        /// </summary>
        public async Task RollbackChanges()
        {
            _transactionState = TransactionState.None;
            await ActiveDataSource.Rollback();
            foreach (var (_, cache) in EntityCaches)
            {
                cache.RollbackTransaction();
            }
            Log.Logger.Information("{Class}: Rolled transaction back", nameof(MapperManager));
        }

        #endregion
    }

    /// <summary>
    /// Represents a transactions state.
    /// </summary>
    internal enum TransactionState
    {
        None,
        Open
    }
}