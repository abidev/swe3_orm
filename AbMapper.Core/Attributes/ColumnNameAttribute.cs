using System;

namespace AbMapper.Core.Attributes
{
    /// <summary>
    /// Specifies the column name used by AbMapper in the database.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ColumnNameAttribute: Attribute
    {
        /// <summary>
        /// Column name to use.
        /// </summary>
        public string Name { get; }
        
        /// <summary>
        /// Sets the column name.
        /// </summary>
        /// <param name="name">Column name to use</param>
        public ColumnNameAttribute(string name)
        {
            Name = name;
        }
    }
}