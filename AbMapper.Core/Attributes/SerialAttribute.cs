using System;

namespace AbMapper.Core.Attributes
{
    /// <summary>
    /// Specifies this property as autoincrement (adds serial modifier to the ddl statement).
    /// Can only be used on integer properties.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class SerialAttribute: Attribute
    {
        
    }
}