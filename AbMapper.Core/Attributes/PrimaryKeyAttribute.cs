using System;

namespace AbMapper.Core.Attributes
{
    /// <summary>
    /// Sets this property as the pk (or part of the pk).
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class PrimaryKeyAttribute: Attribute
    {
        
    }
}