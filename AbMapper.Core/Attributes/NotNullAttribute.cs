using System;

namespace AbMapper.Core.Attributes
{
    /// <summary>
    /// Adds the not null modifier to this columns ddl statement.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class NotNullAttribute: Attribute
    {
        
    }
}