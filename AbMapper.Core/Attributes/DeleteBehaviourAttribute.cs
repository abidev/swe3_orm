using System;
using AbMapper.Core.Models;

namespace AbMapper.Core.Attributes
{
    /// <summary>
    /// Specifies the delete behaviour for references to other entities.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class DeleteBehaviourAttribute: Attribute
    {
        /// <summary>
        /// Delete behaviour to use.
        /// </summary>
        public DeleteBehaviour DeleteBehaviour { get; }
        
        public DeleteBehaviourAttribute(DeleteBehaviour behaviour)
        {
            DeleteBehaviour = behaviour;
        }
    }
}