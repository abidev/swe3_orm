using System;

namespace AbMapper.Core.Attributes
{
    /// <summary>
    /// Excludes the property from being mapped into the database.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ExcludedAttribute: Attribute
    {
        
    }
}