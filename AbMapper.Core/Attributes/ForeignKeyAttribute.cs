using System;

namespace AbMapper.Core.Attributes
{
    /// <summary>
    /// Specifies a relationship to another entity.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ForeignKeyAttribute: Attribute
    {
        /// <summary>
        /// Name of the related table.
        /// </summary>
        public string RelatedTableName { get; }
        /// <summary>
        /// Name of the related property (pk).
        /// </summary>
        public string RelatedPropertyName { get; }
        
        /// <summary>
        /// Sets the name of the related table and property.
        /// </summary>
        /// <param name="relatedTableName">Related table</param>
        /// <param name="relatedPropertyName">Related property</param>
        public ForeignKeyAttribute(string relatedTableName, string relatedPropertyName)
        {
            RelatedTableName = relatedTableName;
            RelatedPropertyName = relatedPropertyName;
        }
    }
}