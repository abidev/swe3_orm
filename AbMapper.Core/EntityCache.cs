using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using AbMapper.Core.Exceptions;
using AbMapper.Core.Models;
using AbMapper.Core.Reflection;
using Serilog;

namespace AbMapper.Core
{
    /// <summary>
    /// Manages cache entries for an entity (specified by type T).
    /// </summary>
    public class EntityCache: IDisposable
    {
        /// <summary>
        /// Metadata for the entity this cache manages.
        /// </summary>
        private readonly Entity _entity;
        
        /// <summary>
        /// Saves cached objects with the hashed pks as key.
        /// </summary>
        private Dictionary<string, object> _entries = new Dictionary<string, object>();

        /// <summary>
        /// Saves hashes of cached objects accessible by hashed pks.
        /// </summary>
        private Dictionary<string, string> _entryHashes = new Dictionary<string, string>();
        
        /// <summary>
        /// Saves cached objects with the hashed pks as key for the current transaction.
        /// </summary>
        private Dictionary<string, object> _entriesTransactional = new Dictionary<string, object>();

        /// <summary>
        /// Saves hashes of cached objects accessible by hashed pks for the current transaction.
        /// </summary>
        private Dictionary<string, string> _entryHashesTransactional = new Dictionary<string, string>();

        /// <summary>
        /// True if any changes happened within the cache during the current transaction.
        /// </summary>
        private bool _hasTransactionChanges = false;
        
        /// <summary>
        /// Used to generate hashes.
        /// </summary>
        private readonly SHA1CryptoServiceProvider _cryptoServiceProvider = new SHA1CryptoServiceProvider();

        /// <summary>
        /// Creates a new cache for the given entity.
        /// </summary>
        /// <param name="entity"></param>
        public EntityCache(Entity entity)
        {
            _entity = entity;
        }

        /// <summary>
        /// Inserts the given instance into cache or overwrites existing instance with same pk values.
        /// </summary>
        /// <param name="entry">Entry to insert into cache.</param>
        /// <param name="entries">Entries dict to insert into.</param>
        /// <param name="entryHashes">Entry hashes dict to insert into.</param>
        public void Insert(object entry, Dictionary<string, object> entries = null, Dictionary<string, string> entryHashes = null)
        {
            if (entry == null) return;
            entries ??= _entries;
            entryHashes ??= _entryHashes;
            
            var hash = GetPkHash(entry);
            var entryHash = GetEntryHash(entry);

            if (entries.ContainsKey(hash))
            {
                if (entryHash != GetEntryHash(entries[hash]))
                {
                    Log.Logger.Error("{Class}: An instance of {Entity} with the desired pk values is already being tracked", nameof(EntityCache), _entity.Type.Name);
                    throw new AbTrackingException("An instance of this entity is already being tracked.");
                }
                
                entries[hash] = entry;
                entryHashes[hash] = entryHash;
                Log.Logger.Verbose("{Class}: Updated cache entry for entity {Entity}", nameof(EntityCache), _entity.Type.Name);
            }
            else
            {
                entries.Add(hash, entry);
                entryHashes.Add(hash, entryHash);
                Log.Logger.Verbose("{Class}: Added cache entry for entity {Entity}", nameof(EntityCache), _entity.Type.Name);
            }
        }

        /// <summary>
        /// Gets cache instance identified by given primary keys.
        /// </summary>
        /// <returns>Cached entry.</returns>
        /// <param name="entries">Entries dict to get from.</param>
        /// <param name="pks">Primary keys to search cache by.</param>
        public object Get(Dictionary<string, object> entries = null, params object[] pks)
        {
            if (pks.Length != _entity.PrimaryKeys.Count)
            {
                Log.Logger.Error("{Class}: Mismatch of primary key count between {Entity} and given pk values", nameof(EntityCache), _entity.Type.Name);
                throw new AbCacheException("Primary key count mismatch");
            }
            entries ??= _entries;

            var hash = GetPkHash(pks);

            return entries.ContainsKey(hash) ? entries[hash] : null;
        }

        /// <summary>
        /// Removes the given instance from cache.
        /// </summary>
        /// <param name="entry">Entry to remove from cache.</param>
        /// <param name="entries">Entries dict to remove from.</param>
        /// <param name="entryHashes">Entry hashes dict to remove from.</param>
        public void Remove(object entry, Dictionary<string, object> entries = null, Dictionary<string, string> entryHashes = null)
        {
            if (entry == null) return;
            entries ??= _entries;
            entryHashes ??= _entryHashes;
            
            var hash = GetPkHash(entry);

            if (!entries.ContainsKey(hash)) return;
            
            entries.Remove(hash);
            entryHashes.Remove(hash);
            Log.Logger.Verbose("{Class}: Removed cache entry for entity {Entity}", nameof(EntityCache), _entity.Type.Name);
        }

        /// <summary>
        /// Iterates over cache entries and returns updated entries.
        /// </summary>
        /// <param name="entries">Entries dict to check for updates.</param>
        /// <param name="entryHashes">Entry hashes dict to check for updates.</param>
        public IReadOnlyCollection<object> GetUpdatedEntries(Dictionary<string, object> entries = null, Dictionary<string, string> entryHashes = null)
        {
            entries ??= _entries;
            entryHashes ??= _entryHashes;
            
            var updatedEntries = new List<object>();
            foreach (var (_, entry) in entries)
            {
                if (HasChanged(entry, entries, entryHashes)) updatedEntries.Add(entry);
            }
            
            Log.Logger.Verbose("{Class}: Checked all {Entity} cache entries for updates", nameof(EntityCache), _entity.Type.Name);

            return updatedEntries;
        }

        #region Transactions
        
        /// <summary>
        /// Inserts the given instance into transaction cache or overwrites existing instance with same pk values.
        /// </summary>
        /// <param name="entry">Entry to insert into cache.</param>
        public void InsertTransactional(object entry)
        {
            _hasTransactionChanges = true;
            Insert(entry, _entriesTransactional, _entryHashesTransactional);
        }

        /// <summary>
        /// Gets transactional cache instance identified by given primary keys.
        /// </summary>
        /// <returns>Cached entry.</returns>
        /// <param name="pks">Primary keys to search cache by.</param>
        public object GetTransactional(params object[] pks)
        {
            return Get(_entriesTransactional, pks);
        }

        /// <summary>
        /// Removes the given instance from transactional cache.
        /// </summary>
        /// <param name="entry">Entry to remove from cache.</param>
        public void RemoveTransactional(object entry)
        {
            _hasTransactionChanges = true;
            Remove(entry, _entriesTransactional, _entryHashesTransactional);
        }
        
        /// <summary>
        /// Initializes the transactional caches.
        /// </summary>
        public void BeginTransaction()
        {
            _entriesTransactional = CloneDict(_entries);
            _entryHashesTransactional = CloneDict(_entryHashes);
            _hasTransactionChanges = false;
            Log.Logger.Verbose("{Class}: Initialized transactional cache for {Entity}", nameof(EntityCache), _entity.Type.Name);
        }

        /// <summary>
        /// Copies the transaction caches to default caches.
        /// </summary>
        public void CommitTransaction()
        {
            _entries = CloneDict(_entriesTransactional);
            _entriesTransactional = null;
            _entryHashes = CloneDict(_entryHashesTransactional);
            _entryHashesTransactional = null;
            _hasTransactionChanges = false;
            Log.Logger.Verbose("{Class}: Committed transactional cache for {Entity}", nameof(EntityCache), _entity.Type.Name);
        }

        /// <summary>
        /// Drops transaction cache.
        /// </summary>
        public void RollbackTransaction()
        {
            _entriesTransactional = null;
            _entryHashesTransactional = null;
            if (_hasTransactionChanges)
            {
                _entries = new Dictionary<string, object>();
                _entryHashes = new Dictionary<string, string>();
                Log.Logger.Verbose("{Class}: Cache was modified during transaction for {Entity}, dropped cache", nameof(EntityCache), _entity.Type.Name);
            }
            
            _hasTransactionChanges = false;
        }

        #endregion

        /// <summary>
        /// Deep copies a dict into another one.
        /// https://stackoverflow.com/questions/139592/what-is-the-best-way-to-clone-deep-copy-a-net-generic-dictionarystring-t
        /// </summary>
        private static Dictionary<TKey, TValue> CloneDict<TKey, TValue>(Dictionary<TKey, TValue> dict)
        {
            var ret = new Dictionary<TKey, TValue>(dict.Count, dict.Comparer);
            
            foreach (KeyValuePair<TKey, TValue> entry in dict)
            {
                ret.Add(entry.Key, entry.Value);
            }
            
            return ret;
        }

        /// <summary>
        /// Checks if the given entry has changed since last cache update.
        /// </summary>
        /// <param name="entry">Entry to check for changes.</param>
        /// <param name="entries">Entries dict to check for updates.</param>
        /// <param name="entryHashes">Entry hashes dict to check for updates.</param>
        /// <returns>True if entry is cached and has changed, false otherwise.</returns>
        public bool HasChanged(object entry, Dictionary<string, object> entries = null, Dictionary<string, string> entryHashes = null)
        {
            if (entry == null) return false;
            entries ??= _entries;
            entryHashes ??= _entryHashes;
            
            var hash = GetPkHash(entry);
            
            if (!entries.ContainsKey(hash)) return false;

            var entryHash = GetEntryHash(entry);
            return entryHash != entryHashes[hash];
        }

        /// <summary>
        /// Creates a unique hash for the given entry's primary key values.
        /// </summary>
        /// <param name="pks">Primary keys to create hash for.</param>
        /// <returns>Hash.</returns>
        private string GetPkHash(params object[] pks)
        {
            if (pks.Length != _entity.PrimaryKeys.Count) throw new AbCacheException("Primary key count mismatch");
            
            var hashBase = "";
            for (var i = 0; i < pks.Length; i++)
            {
                hashBase += $"{_entity.PrimaryKeys[i].ColumnName}={pks[i]}";
            }

            return Encoding.UTF8.GetString(_cryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(hashBase)));
        }

        /// <summary>
        /// Creates a unique hash for the given entry's primary key values.
        /// </summary>
        /// <param name="entry">Entry to create hash for.</param>
        /// <returns>Hash.</returns>
        private string GetPkHash(object entry)
        {
            var hashBase = "";
            foreach (var pkProperty in _entity.PrimaryKeys)
            {
                hashBase += $"{pkProperty.ColumnName}={pkProperty.PropertyInfo.GetValue(entry)}";
            }

            return Encoding.UTF8.GetString(_cryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(hashBase)));
        }

        /// <summary>
        /// Creates a unique hash for the given entry's instance values.
        /// </summary>
        /// <param name="entry">Entry to create hash for.</param>
        /// <param name="entity">Set if the entry to be hashed is of a different entity.</param>
        /// <returns>Hash.</returns>
        private string GetEntryHash(object entry, Entity entity = null)
        {
            var hashBase = "";
            foreach (var property in (entity ?? _entity).Properties.Where(p => p.ExistsOnEntityType))
            {
                var info = (entity ?? _entity).ForeignEntityInfos.FirstOrDefault(feInfo =>
                    feInfo.ForeignEntityProperty == property);
                object value = null;
                if (!Util.IsCollectionOfType(info?.ForeignEntityProperty?.Type, typeof(RelatedCollection<>)))
                {
                    value = property.PropertyInfo.GetValue(entry);
                }
                
                if (info != null && value != null)
                {
                    hashBase += $"{property.ColumnName}={GetEntryHash(value, info.ForeignEntity)}";
                }
                else if (!property.IsExcluded)
                {
                    hashBase += $"{property.ColumnName}={value}";
                }
            }

            return Encoding.UTF8.GetString(_cryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(hashBase)));
        }

        public void Dispose()
        {
            _cryptoServiceProvider.Dispose();
        }
    }
}