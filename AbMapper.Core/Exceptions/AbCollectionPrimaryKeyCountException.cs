using System;

namespace AbMapper.Core.Exceptions
{
    /// <summary>
    /// Thrown if no primary key is specified on a registered entity.
    /// </summary>
    public class AbCollectionPrimaryKeyCountException: Exception
    {
        public AbCollectionPrimaryKeyCountException() {}
        public AbCollectionPrimaryKeyCountException(string msg): base(msg) {}
        public AbCollectionPrimaryKeyCountException(string msg, Exception inner): base(msg, inner) {}
    }
}