using System;

namespace AbMapper.Core.Exceptions
{
    /// <summary>
    /// Generic exception thrown by a datasource if ddl/dml tasks cannot be performed.
    /// </summary>
    public class AbDataSourceException: Exception
    {
        public AbDataSourceException(): base() {}
        public AbDataSourceException(string msg): base(msg) {}
    }
}