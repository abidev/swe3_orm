using System;

namespace AbMapper.Core.Exceptions
{
    /// <summary>
    /// Thrown when an error occurs while trying to track an entry.
    /// </summary>
    public class AbTrackingException: Exception
    {
        public AbTrackingException(string msg): base(msg) {}
    }
}