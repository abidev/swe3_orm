using System;

namespace AbMapper.Core.Exceptions
{
    /// <summary>
    /// Thrown by querybuilder classes if an illegal operation is performed.
    /// </summary>
    public class AbQueryBuilderException: Exception
    {
        public AbQueryBuilderException(): base() {}
        public AbQueryBuilderException(string msg): base(msg) {}
    }
}