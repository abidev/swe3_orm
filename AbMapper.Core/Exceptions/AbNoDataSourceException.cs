using System;

namespace AbMapper.Core.Exceptions
{
    /// <summary>
    /// Thrown if no datasource is configured and AbMapper is initialized.
    /// </summary>
    public class AbNoDataSourceException: Exception
    {
        public AbNoDataSourceException(): base() {}
        public AbNoDataSourceException(string msg): base(msg) {}
        public AbNoDataSourceException(string msg, Exception inner): base(msg, inner) {}
    }
}