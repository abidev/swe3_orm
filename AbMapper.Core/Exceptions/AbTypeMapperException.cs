using System;

namespace AbMapper.Core.Exceptions
{
    /// <summary>
    /// Thrown if there is an error mapping c# types to datasource types. (eg. type not found)
    /// </summary>
    public class AbTypeMapperException: Exception
    {
        public AbTypeMapperException(): base() {}
        public AbTypeMapperException(string msg): base(msg) {}
    }
}