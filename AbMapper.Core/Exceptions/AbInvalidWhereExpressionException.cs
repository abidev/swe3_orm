using System;

namespace AbMapper.Core.Exceptions
{
    /// <summary>
    /// Thrown when an invalid where expression is applied to a query.
    /// </summary>
    public class AbInvalidWhereExpressionException: Exception
    {
        public AbInvalidWhereExpressionException(): base() {}
        public AbInvalidWhereExpressionException(string msg): base(msg) {}
    }
} 