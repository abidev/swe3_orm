using System;

namespace AbMapper.Core.Exceptions
{
    /// <summary>
    /// Thrown by cache in case of primary key count mismatches with the cache entity.
    /// </summary>
    public class AbCacheException: Exception
    {
        public AbCacheException(): base() {}
        
        public AbCacheException(string msg): base(msg) {}
    }
}