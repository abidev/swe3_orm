using System;

namespace AbMapper.Core.Exceptions
{
    /// <summary>
    /// Generic exception thrown if AbMapper gets into erroneous state. 
    /// </summary>
    public class AbMapperException: Exception
    {
        public AbMapperException(): base() {}
        public AbMapperException(string msg): base(msg) {}
    }
}