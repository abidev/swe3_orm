using System.Threading.Tasks;
using AbMapper.Core.Interfaces;

namespace AbMapper.Core
{
    /// <summary>
    /// Registers entity collections to be managed by AbMapper.
    /// Provides frameworks wide features to the user.
    /// </summary>
    public abstract class DataContext
    {
        /// <summary>
        /// Registers a config (with a datasource) with AbMapper.
        /// Starts AbMapper initialization.
        /// </summary>
        /// <param name="config"></param>
        protected DataContext(IDataSourceConfig config)
        {
            MapperInitializer.Instance.InitializeMapper(config, this);
        }

        /// <summary>
        /// Persists tracked change list to datasource.
        /// </summary>
        public async Task SaveChangesAsync()
        {
            await MapperManager.Instance.SaveChangesAsync();
        }

        /// <summary>
        /// Clears all cached objects.
        /// </summary>
        public void ClearCache()
        {
            MapperManager.Instance.ClearCaches();
        }

        /// <summary>
        /// Checks if the given entry has changed.
        /// </summary>
        /// <param name="entry">Entry to check for changes.</param>
        /// <returns>True if entry has changed, false otherwise.</returns>
        public bool HasChanged(object entry)
        {
            return MapperManager.Instance.HasEntryChanged(entry);
        }

        /// <summary>
        /// Untracks the given entry. This deletes the entry from cache and allows a new entry to be tracked.
        /// </summary>
        /// <param name="entry">Entry to untrack.</param>
        public void Untrack(object entry)
        {
            MapperManager.Instance.UntrackEntry(entry);
        }

        /// <summary>
        /// Starts a new transaction containing the following insert, update or delete statements.
        /// </summary>
        public async Task BeginTransaction()
        {
            await MapperManager.Instance.BeginTransaction();
        }

        /// <summary>
        /// Commits the transactions changes.
        /// </summary>
        public async Task CommitChanges()
        {
            await MapperManager.Instance.CommitChanges();
        }

        /// <summary>
        /// Rollbacks the transactions changes.
        /// </summary>
        public async Task RollbackChanges()
        {
            await MapperManager.Instance.RollbackChanges();
        }
    }
}