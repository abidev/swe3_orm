using System;
using AbMapper.Core.Models;

namespace AbMapper.Core.Interfaces
{
    /// <summary>
    /// Represents a datasource change that will be persisted when SaveChanges is called.
    /// </summary>
    public interface IDbChange
    {
        /// <summary>
        /// Type of the change. (create, update, delete)
        /// </summary>
        public DbChangeType ChangeType { get; set; }
        
        /// <summary>
        /// Type of the data to be changed.
        /// </summary>
        public Type Type { get; }
        
        /// <summary>
        /// Data to be persisted on save.
        /// </summary>
        public object Data { get; set; }
    }
}