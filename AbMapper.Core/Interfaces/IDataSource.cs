using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AbMapper.Core.Models;
using AbMapper.Core.Reflection;

namespace AbMapper.Core.Interfaces
{
    /// <summary>
    /// Implements ddl and dml operations against a datasource (eg. postgresql).
    /// </summary>
    public interface IDataSource
    {
        /// <summary>
        /// Initialization of the datasource. Called once during AbMapper initialization.
        /// </summary>
        public Task InitializeAsync();
        
        /// <summary>
        /// Called after all entities are in sync with AbMapper metadata. Can be used to setup relationships.
        /// </summary>
        public Task SetupRelationshipsAsync();
        
        /// <summary>
        /// Syncs an entity against the datasource. (adjusts the structure, ddl)
        /// </summary>
        /// <param name="entity">Entity to sync</param>
        public Task SyncEntityAsync(Entity entity);
        
        /// <summary>
        /// Persists entity data to the datasource.
        /// </summary>
        /// <param name="entity">Entity with metadata (including the given values' type)</param>
        /// <param name="data">Object holding data to be persisted</param>
        public Task InsertAsync(Entity entity, object data);

        /// <summary>
        /// Removes entity data from the datasource.
        /// </summary>
        /// <param name="entity">Entity with metadata</param>
        /// <param name="data">Object holding data to be removed</param>
        public Task RemoveAsync(Entity entity, object data);

        /// <summary>
        /// Queries an entry of the given entity.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="foreignEntityInfos"></param>
        /// <param name="whereExpressions"></param>
        /// <param name="pkValues">Primary key values to search for.</param>
        /// <typeparam name="T"></typeparam>
        /// <returns>
        ///    Desired entry object: If found.
        ///    T default (eg. null): If not found.
        /// </returns>
        public Task<T> GetAsync<T>(Entity entity, IEnumerable<ForeignEntityInfo> foreignEntityInfos = null,
            IEnumerable<BinaryExpression> whereExpressions = null, params object[] pkValues) where T : class, new();

        /// <summary>
        /// Queries all entries of the given entity.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="eagerLoad"></param>
        /// <param name="foreignEntityInfos"></param>
        /// <param name="whereExpressions"></param>
        /// <param name="limit"></param>
        /// <param name="skip"></param>
        /// <param name="orderByInfo"></param>
        /// <typeparam name="T"></typeparam>
        public Task<IEnumerable<T>> GetAllAsync<T>(Entity entity, bool eagerLoad = false, IEnumerable<ForeignEntityInfo> foreignEntityInfos = null, 
            IEnumerable<BinaryExpression> whereExpressions = null, int? limit = null, int? skip = null, OrderByInfo orderByInfo = null) where T: class, new();

        /// <summary>
        /// Starts a transaction.
        /// </summary>
        public Task Begin();

        /// <summary>
        /// Commits transaction changes.
        /// </summary>
        public Task Commit();

        /// <summary>
        /// Rollbacks transaction changes.
        /// </summary>
        public Task Rollback();
    }
}