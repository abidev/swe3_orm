namespace AbMapper.Core.Interfaces
{
    /// <summary>
    /// Provides an api to build a SQL query string.
    /// </summary>
    public interface IQueryBuilder
    {
        /// <summary>
        /// Finishes the SQL query string. After Finish is called no modifications the the query can be made.
        /// Can be called multiple times.
        /// </summary>
        public string Finish();
    }
}