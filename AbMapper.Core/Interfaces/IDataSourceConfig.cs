using System;
using Serilog.Events;

namespace AbMapper.Core.Interfaces
{
    /// <summary>
    /// Holds configuration for a datasource.
    /// </summary>
    public interface IDataSourceConfig
    {
        /// <summary>
        /// Database connection string.
        /// </summary>
        public string ConnectionString { get; set; }
        
        /// <summary>
        /// Type of the datasource to be used with this config. AbMapper uses this to get an instance of the datasource.
        /// </summary>
        public Type DataSourceType { get; }
        
        /// <summary>
        /// If true, columns that are removed from entity classes are also dropped from the datasource.
        /// (results in data loss)
        /// </summary>
        public bool DropRemovedColumns { get; set; }
        
        /// <summary>
        /// Completely rebuild the datasource structure on AbMapper startup. Used for development purposes.
        /// </summary>
        public bool WipeDatabaseOnStartup { get; set; }

        /// <summary>
        /// Enables caching for entity instances.
        /// </summary>
        public bool EnableCaching { get; set; }
        
        /// <summary>
        /// AbMapper log level.
        /// </summary>
        public LogEventLevel LogLevel { get; set; }
    }
}