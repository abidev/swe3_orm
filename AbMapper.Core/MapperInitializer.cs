using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AbMapper.Core.Exceptions;
using AbMapper.Core.Interfaces;
using AbMapper.Core.Models;
using AbMapper.Core.Reflection;
using Serilog;

namespace AbMapper.Core
{
    /// <summary>
    /// Manages initialization process of AbMapper.
    /// </summary>
    internal class MapperInitializer
    {
        private static readonly Lazy<MapperInitializer> LazyInstance = new Lazy<MapperInitializer>(() => new MapperInitializer());
        public static MapperInitializer Instance => LazyInstance.Value;
        
        /// <summary>
        /// Currently active datasource.
        /// </summary>
        private IDataSource ActiveDataSource { get; set; }
        
        /// <summary>
        /// Currently active datacontext.
        /// </summary>
        private DataContext ActiveDataContext { get; set; }
        
        /// <summary>
        /// List of discovered entities with analyzed metadata.
        /// </summary>
        private Dictionary<Type, Entity> Entities { get; set; }

        static MapperInitializer()
        {
            
        }
        
        /// <summary>
        /// Initializes AbMapper by syncing registered entities with the datasource specified in config.
        /// </summary>
        /// <param name="config"></param>
        /// <param name="context"></param>
        public void InitializeMapper(IDataSourceConfig config, DataContext context)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Is(config.LogLevel)
                .WriteTo.Console()
                .CreateLogger();
            RebuildDataSource(config);
            SetupDatabaseAsync(context).Wait();

            MapperManager.Instance.ClearCaches();
            MapperManager.Instance.UseInitializationResults(ActiveDataSource, config, ActiveDataContext, Entities);
            
            Log.Logger.Information("{Class}: Initialization complete", nameof(MapperInitializer));
        }
        
        /// <summary>
        /// Configures an instance of the configured datasource class during initialization.
        /// </summary>
        /// <param name="config"></param>
        private void RebuildDataSource(IDataSourceConfig config)
        {
            ActiveDataSource = (IDataSource) Activator.CreateInstance(config.DataSourceType, config);
            
            Log.Logger.Debug("{Class}: Loaded configured data source - {Source}", nameof(MapperInitializer), config.DataSourceType.Name);
        }

        /// <summary>
        /// Initializes the configured datasource and syncs entities.
        /// </summary>
        /// <param name="context"></param>
        private async Task SetupDatabaseAsync(DataContext context)
        {
            ActiveDataContext = context ?? throw new ArgumentNullException(nameof(context), "DataContext cannot be null.");
            GatherCollectionTypes(context.GetType().GetProperties());
            
            await ActiveDataSource.InitializeAsync();
            await SyncTablesWithDataSourceAsync();
            
            Log.Logger.Debug("{Class}: Finished db synchronization", nameof(MapperInitializer));
        }

        /// <summary>
        /// Collects and analyses registered DbCollection types in the configured DbContext.
        /// </summary>
        /// <param name="propertyInfos"></param>
        private void GatherCollectionTypes(PropertyInfo[] propertyInfos)
        {
            Entities = new Dictionary<Type, Entity>();
            var navigationEntities = new List<Entity>();
            foreach (var propertyInfo in propertyInfos)
            {
                // Check for AbCollection types
                if (propertyInfo.PropertyType.IsGenericType)
                {
                    var collectionType = propertyInfo.PropertyType.GenericTypeArguments[0];
                    // Check for parameterless constructor (DbCollection type param has new() constraint) and check if prop is a DbCollection
                    if (collectionType.GetConstructor(Type.EmptyTypes) != null && propertyInfo.PropertyType == typeof(DbCollection<>).MakeGenericType(collectionType))
                    {
                        RegisterCollectionType(collectionType);
                    }
                }
            }

            // Save entity relationship information
            foreach (var (_, entity) in Entities)
            {
                var relatedEntities = new List<ForeignEntityInfo>();
                foreach (var property in entity.Properties)
                {
                    /* 1:n relationship, 1 side */
                    var relatedEntity = Util.FindEntityByTypeOrDefault(property.Type, Entities);
                    
                    if (relatedEntity != null) // Found a related entity
                    {
                        var info = BuildForeignEntity1ToNInfo(relatedEntity, property);
                        
                        relatedEntities.Add(info);
                        if (!entity.HasRelatedEntity) entity.HasRelatedEntity = true;
                    }
                    
                    /* 1:n relationship, n side */
                    if (relatedEntity != null ||
                        !Util.IsCollectionOfType(property.Type, typeof(RelatedCollection<>))) continue;
                    
                    relatedEntity = Util.FindEntityByTypeOrDefault(property.Type.GenericTypeArguments[0], Entities);

                    if (relatedEntity == null) continue;
                    
                    var reverseRelationProperty = relatedEntity.Properties.FirstOrDefault(p => p.Type == entity.Type);
                    
                    if (reverseRelationProperty != null) // 1 side of relationship is also defined
                    {
                        var info = new ForeignEntityInfo { ForeignEntity = relatedEntity, ForeignEntityProperty = property, ForeignEntityPropertyIsCollection = true };
                        relatedEntities.Add(info);
                        
                        if (!entity.HasRelatedEntity) entity.HasRelatedEntity = true;
                        
                        property.IsExcluded = true;
                        property.DeleteBehaviour ??= reverseRelationProperty.DeleteBehaviour;
                    }
                    
                    /* n:m relationship */
                    var reverseRelationCollectionProperty = relatedEntity.Properties.FirstOrDefault(p =>
                        Util.IsCollectionOfType(p.Type, typeof(RelatedCollection<>)) && p.Type.GenericTypeArguments[0] == entity.Type);

                    if (reverseRelationCollectionProperty == null) continue;
                    
                    var entityFeInfo = BuildForeignEntityNToMInfo(entity, relatedEntity, navigationEntities,
                        property, reverseRelationCollectionProperty);
                        
                    relatedEntities.Add(entityFeInfo);
                        
                    if (!entity.HasRelatedEntity) entity.HasRelatedEntity = true;
                    property.IsExcluded = true;
                }

                foreach (var relatedEntity in relatedEntities)
                {
                    entity.AddRelatedEntity(relatedEntity);
                }
            }
            
            foreach (var entity in navigationEntities)
            {
                Entities.Add(entity.Type, entity);   
            }
        }

        /// <summary>
        /// Builds foreign entity info for a given foreign entity and the property of the original entity that points to the foreign entity.
        /// </summary>
        /// <param name="foreignEntity"></param>
        /// <param name="property"></param>
        /// <returns></returns>
        private static ForeignEntityInfo BuildForeignEntity1ToNInfo(Entity foreignEntity, Property property)
        {
            var info = new ForeignEntityInfo
            {
                ForeignEntity = foreignEntity,
                ForeignEntityProperty = property
            };
            foreach (var pkProp in foreignEntity.PrimaryKeys)
            {
                var fkProp = new Property
                {
                    Name = property.Name + pkProp.Name,
                    IsPrimary = false,
                    IsForeign = true,
                    Type = pkProp.Type,
                    IsSerial = false,
                    NotNull = property.NotNull,
                    IsExcluded = false,
                    ExistsOnEntityType = false,
                    PropertyInfo = pkProp.PropertyInfo,
                    DeleteBehaviour = property.DeleteBehaviour
                };

                fkProp.ColumnName = fkProp.Name.ToLower();
                var fkInfo = new ForeignKeyInfo
                {
                    ForeignProperty = pkProp,
                    LocalProperty = fkProp
                };
                info.AddForeignKeyInfo(fkInfo);
            }
                        
            // mark references to other entities as excluded to not persist to db
            property.IsExcluded = true;

            return info;
        }
        
        private static ForeignEntityInfo BuildForeignEntityNToMInfo(Entity entity, Entity reverseEntity, List<Entity> navigationEntities, Property property, Property reverseProperty)
        {
            var navEntity = navigationEntities.Where(e => e.IsMapperManaged).FirstOrDefault(e =>
                            e.TableName == $"{entity.TableName}_{reverseEntity.TableName}" ||
                            e.TableName == $"{reverseEntity.TableName}_{entity.TableName}");

            if (navEntity == null)
            {
                navEntity = new Entity {
                    Type = typeof(ExpandoObject),
                    TableName = $"{entity.TableName}_{reverseEntity.TableName}",
                    HasRelatedEntity = true,
                    HasPrimaryKey = true,
                    IsMapperManaged = true
                };
                navigationEntities.Add(navEntity);
            }

            var info = new ForeignEntityInfo { ForeignEntity = entity };
            foreach (var pkProp in entity.PrimaryKeys)
            {
                var fkProp = new Property
                {
                    Name = reverseProperty.Name + pkProp.Name,
                    IsPrimary = true,
                    IsForeign = true,
                    Type = pkProp.Type,
                    IsSerial = false,
                    NotNull = true,
                    IsExcluded = false,
                    PropertyInfo = pkProp.PropertyInfo,
                    DeleteBehaviour = property.DeleteBehaviour
                };

                fkProp.ColumnName = fkProp.Name.ToLower();
                var fkInfo = new ForeignKeyInfo
                {
                    ForeignProperty = pkProp,
                    LocalProperty = fkProp
                };
                info.AddForeignKeyInfo(fkInfo);
            }
            navEntity.AddRelatedEntity(info);
            
            // Add info of the original entities relationship
            var entityFeInfo = new ForeignEntityInfo { ForeignEntity = reverseEntity, ForeignEntityProperty = property, ForeignEntityPropertyIsCollection = true, NavigationEntity = navEntity };
            return entityFeInfo;
        }
        

        /// <summary>
        /// Analyses and registers metadata of the given type.
        /// </summary>
        /// <param name="type"></param>
        private void RegisterCollectionType(Type type)
        {
            Entities.Add(type, new Entity(type));
            Log.Logger.Verbose("{Class}: Registered type {Type}", nameof(MapperInitializer), type.Name);
        }

        /// <summary>
        /// Syncs registered entities with datasource.
        /// </summary>
        /// <exception cref="AbNoDataSourceException">Thrown if no datasource is configured</exception>
        private async Task SyncTablesWithDataSourceAsync()
        {
            if (ActiveDataSource == null) throw new AbNoDataSourceException("No DataSource configured");

            foreach (var (_, entity) in Entities)
            {
                await ActiveDataSource.SyncEntityAsync(entity);
            }

            await ActiveDataSource.SetupRelationshipsAsync();
        }
    }
}