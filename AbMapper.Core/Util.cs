using System;
using System.Collections.Generic;
using AbMapper.Core.Exceptions;
using AbMapper.Core.Reflection;

namespace AbMapper.Core
{
    /// <summary>
    /// AbMapper utilities.
    /// </summary>
    public static class Util
    {
        /// <summary>
        /// Checks if a type is the given collection type.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="collectionType"></param>
        public static bool IsCollectionOfType(Type type, Type collectionType)
        {
            if (type == null || !type.IsGenericType || !collectionType.IsGenericType) return false;
            
            var genericType = type.GenericTypeArguments[0];

            // Check for parameterless constructor (DbCollection type param has new() constraint) and check if type is of given collection type
            return genericType.GetConstructor(Type.EmptyTypes) != null &&
                   type == collectionType.MakeGenericType(genericType);
        }

        /// <summary>
        /// Finds the entity with the given type within the given list.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="entities"></param>
        /// <returns>Entity with Type = given type</returns>
        public static Entity FindEntityByType(Type type, Dictionary<Type, Entity> entities)
        {
            if (!entities.ContainsKey(type)) throw new AbMapperException("No entity registered for this type");

            return entities[type];
        }

        /// <summary>
        /// Finds the entity by type within the given dict. 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static Entity FindEntityByTypeOrDefault(Type type, Dictionary<Type, Entity> entities)
        {
            return entities.ContainsKey(type) ? entities[type] : null;
        }
    }
}