using System.Collections.Generic;

namespace AbMapper.Core.Models
{
    /// <summary>
    /// Represents a collection of related entities of a type. Used to query related data.
    /// </summary>
    /// <typeparam name="T">Type of the related entity</typeparam>
    public class RelatedCollection<T>: List<T> where T: class
    {
        
    }
}