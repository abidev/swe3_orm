using System.Reflection;

namespace AbMapper.Core.Models
{
    /// <summary>
    /// Holds info needed to add order to get all calls.
    /// </summary>
    public class OrderByInfo
    {
        /// <summary>
        /// The member to order by.
        /// </summary>
        public MemberInfo OrderByMember { get; set; }

        /// <summary>
        /// OrderBy direction.
        /// </summary>
        public OrderByDirection Direction { get; set; } = OrderByDirection.Ascending;
    }
}