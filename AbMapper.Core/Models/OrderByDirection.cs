namespace AbMapper.Core.Models
{
    /// <summary>
    /// Models order by direction for getall calls.
    /// </summary>
    public enum OrderByDirection
    {
        Ascending,
        Descending
    }
}