using System;
using AbMapper.Core.Interfaces;

namespace AbMapper.Core.Models
{
    public class DbChange: IDbChange
    {
        /// <summary>
        /// Type of the change. (create, update, delete)
        /// </summary>
        public DbChangeType ChangeType { get; set; }

        /// <summary>
        /// Type of the data to be changed.
        /// </summary>
        public Type Type => Data.GetType();

        /// <summary>
        /// Data to be persisted on save.
        /// </summary>
        public object Data { get; set; }

        /// <summary>
        /// Sets change type and data to be persisted.
        /// </summary>
        /// <param name="changeType"></param>
        /// <param name="data"></param>
        public DbChange(DbChangeType changeType, object data)
        {
            ChangeType = changeType;
            Data = data;
        }
    }

    /// <summary>
    /// Supported types of changes.
    /// </summary>
    public enum DbChangeType
    {
        Insert,
        Update,
        Delete
    }
}