namespace AbMapper.Core.Models
{
    public enum DeleteBehaviour
    {
        /// <summary>
        /// DEFAULT
        /// Raises an error other rows have references to this row.
        /// </summary>
        NoAction,
        
        /// <summary>
        /// Prevents the deletion if other rows have references to this row.
        /// </summary>
        Restrict,
        
        /// <summary>
        /// Cascades the deletion by deleting other rows that have references to this row.
        /// </summary>
        Cascade,
        
        /// <summary>
        /// Sets fks pointing to the deleted entry null if possible, fails otherwise.
        /// </summary>
        SetNull
    }
}