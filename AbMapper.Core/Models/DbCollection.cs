using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using AbMapper.Core.Exceptions;

namespace AbMapper.Core.Models
{
    /// <summary>
    /// Represents a collection of entities to be managed by AbMapper.
    /// </summary>
    /// <typeparam name="T">Entity type</typeparam>
    public class DbCollection<T>: List<T> where T: class, new()
    {
        /// <summary>
        /// A list of navigation members that should be included when querying this entity.
        /// </summary>
        private readonly List<MemberInfo> _includedNavigationMembers = new List<MemberInfo>();

        /// <summary>
        /// A list of binary expression that constrain the result set of where operations.
        /// </summary>
        private readonly List<BinaryExpression> _whereExpressions = new List<BinaryExpression>();

        /// <summary>
        /// Holds order by information.
        /// </summary>
        private OrderByInfo _orderByInfo = null;

        /// <summary>
        /// Select query returned row number limit.
        /// </summary>
        private int? _whereLimit = null;

        /// <summary>
        /// Select query skipped row count.
        /// </summary>
        private int? _whereSkip = null;
        
        /// <summary>
        /// Gets an instance of this entity.
        /// </summary>
        /// <param name="pks">Array of primary key values to search by.</param>
        /// <returns></returns>
        public async Task<T> GetAsync(params object[] pks)
        {
            var res = await MapperManager.Instance.GetAsync<T>(_includedNavigationMembers, _whereExpressions, pks);
            _includedNavigationMembers.Clear();
            _whereExpressions.Clear();
            return res;
        }
        
        /// <summary>
        /// Gets a list of all instances of this entity.
        /// </summary>
        /// <returns>All instances of this entity</returns>
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var res = await MapperManager.Instance.GetAllAsync<T>(_includedNavigationMembers, _whereExpressions, 
                limit: _whereLimit, skip: _whereSkip, orderByInfo: _orderByInfo);
            _includedNavigationMembers.Clear();
            _whereExpressions.Clear();
            _whereSkip = null;
            _whereLimit = null;
            _orderByInfo = null;
            return res;
        }

        /// <summary>
        /// Inserts a new instance of this entity.
        /// </summary>
        /// <param name="data">Entity with data to insert</param>
        public void Save(T data)
        {
            MapperManager.Instance.InsertData(data);
        }

        /// <summary>
        /// Deletes the given instance from db.
        /// </summary>
        /// <param name="data">Instance to delete.</param>
        public void Delete(T data)
        {
            MapperManager.Instance.DeleteData(data);
        }
        
        /// <summary>
        /// Marks a member of the queried entity to include related data.
        /// </summary>
        /// <param name="selectorExpr"></param>
        /// <typeparam name="TInclude"></typeparam>
        public DbCollection<T> Load<TInclude>(Expression<Func<T, TInclude>> selectorExpr)
        {
            if (selectorExpr.Body is MemberExpression res) 
                _includedNavigationMembers.Add(res.Member);
            return this;
        }
        
        /// <summary>
        /// Adds a constraint to the queried data.
        /// </summary>
        /// <param name="comparisonExpr"></param>
        /// <returns></returns>
        public DbCollection<T> Where(Expression<Func<T, bool>> comparisonExpr)
        {
            if (comparisonExpr.Body is BinaryExpression expr && 
                IsAllowedWhereExpression(expr.Left) && IsAllowedWhereExpression(expr.Right))
                _whereExpressions.Add(expr);
            else
                throw new AbInvalidWhereExpressionException($"Expression {comparisonExpr.Body} (Type: {comparisonExpr.Body.Type}) is not a BinaryExpression consisting of Constant- Unary- and PropertyExpressions");
            return this;
        }

        /// <summary>
        /// Sets the max number of returned entries.
        /// </summary>
        /// <returns></returns>
        public DbCollection<T> Limit(int? limit)
        {
            _whereLimit = limit;
            return this;
        }

        /// <summary>
        /// Sets the number of skipped rows.
        /// </summary>
        /// <returns></returns>
        public DbCollection<T> Skip(int? skip)
        {
            _whereSkip = skip;
            return this;
        }

        /// <summary>
        /// Sets the property to order by.
        /// </summary>
        /// <param name="orderByExpr">Member expression selecting the member to order by.</param>
        /// <param name="direction">Order by direction, defaults to ascending.</param>
        /// <typeparam name="TOrder"></typeparam>
        /// <returns></returns>
        public DbCollection<T> OrderBy<TOrder>(Expression<Func<T, TOrder>> orderByExpr, OrderByDirection direction = OrderByDirection.Ascending)
        {
            if (orderByExpr.Body is MemberExpression res)
                _orderByInfo = new OrderByInfo { OrderByMember = res.Member, Direction = direction };
            return this;
        }
        
        private bool IsAllowedWhereExpression(Expression expr)
        {
            return expr is MemberExpression || expr is ConstantExpression || expr is UnaryExpression;
        }
    }
}