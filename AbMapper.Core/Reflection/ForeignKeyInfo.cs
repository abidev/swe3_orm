using System;

namespace AbMapper.Core.Reflection
{
    /// <summary>
    /// Stores information about a fk relationship to a pk.
    /// </summary>
    public class ForeignKeyInfo
    {
        // public Entity ForeignEntity { get; set; }
        public Property ForeignProperty { get; set; }
        public Property LocalProperty { get; set; }
    }
}