using System;
using System.Reflection;
using AbMapper.Core.Attributes;
using AbMapper.Core.Models;

namespace AbMapper.Core.Reflection
{
    /// <summary>
    /// Stores metadata of a property belonging to an entity.
    /// </summary>
    public class Property
    {
        /// <summary>
        /// Property type.
        /// </summary>
        public Type Type { get; set; }
        
        /// <summary>
        /// Property name in entity type.
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Property column name to be used when persisting to datasource. Defaults to prop name in entity class.
        /// (ColumnName attribute)
        /// </summary>
        public string ColumnName { get; set; }
        
        /// <summary>
        /// True if this property is the primary key or part of the primary key.
        /// (PrimaryKey attribute)
        /// </summary>
        public bool IsPrimary { get; set; }
        
        /// <summary>
        /// True if this property is to be automatically incremented by the datasource. Can only be used on int properties)
        /// (Serial attribute)
        /// </summary>
        public bool IsSerial { get; set; }
        
        /// <summary>
        /// True if this property should not be able to persist null to datasource.
        /// (NotNull attribute)
        /// </summary>
        public bool NotNull { get; set; }
        
        /// <summary>
        /// True if this property should be ignored by AbMapper.
        /// (Excluded attribute)
        /// </summary>
        public bool IsExcluded { get; set; }
        
        /// <summary>
        /// True if this property is a foreign key to another entity.
        /// (ForeignKey attribute)
        /// </summary>
        public bool IsForeign { get; set; }

        /// <summary>
        /// Delete behaviour to use. (if this prop is foreign)
        /// </summary>
        public DeleteBehaviour? DeleteBehaviour { get; set; } = null;
        
        /// <summary>
        /// True if this property is an enum type.
        /// </summary>
        public bool IsEnum { get; set; }

        /// <summary>
        /// True if this property exists on the entity's type.
        /// </summary>
        public bool ExistsOnEntityType { get; set; } = true;
        
        /// <summary>
        /// Stores this property's info.
        /// </summary>
        public PropertyInfo PropertyInfo { get; set; }
    }
}