using System.Collections.Generic;

namespace AbMapper.Core.Reflection
{
    /// <summary>
    /// Stores information about a relationship to another entity.
    /// </summary>
    public class ForeignEntityInfo
    {
        /// <summary>
        /// The entity that is used to navigate to the foreign entity for n:m relationships.
        /// Null if relationship is not n:m.
        /// </summary>
        public Entity NavigationEntity { get; set; }
        
        /// <summary>
        /// The related entity.
        /// </summary>
        public Entity ForeignEntity { get; set; }
        
        /// <summary>
        /// True if the local entity has columns for the fks (so for the related pks). TODO: unused
        /// </summary>
        public bool HasEntityProperties { get; set; }
        
        /// <summary>
        /// The local property which references the related entity.
        /// </summary>
        public Property ForeignEntityProperty { get; set; }
        
        /// <summary>
        /// Indicates that a foreign entity property is a collection. This is the case for 1:n n side or n:m.
        /// </summary>
        public bool ForeignEntityPropertyIsCollection { get; set; }

        /// <summary>
        /// Stores information about the local fks and which foreign pks they relate to. (can be auto gen. or man.)
        /// </summary>
        public IEnumerable<ForeignKeyInfo> ForeignKeyInfos => _foreignKeyInfos.AsReadOnly();
        
        private readonly List<ForeignKeyInfo> _foreignKeyInfos = new List<ForeignKeyInfo>();

        /// <summary>
        /// Adds information about a local fk.
        /// </summary>
        /// <param name="info"></param>
        public void AddForeignKeyInfo(ForeignKeyInfo info)
        {
            _foreignKeyInfos.Add(info);
        }
    }
}