using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AbMapper.Core.Attributes;
using AbMapper.Core.Exceptions;

namespace AbMapper.Core.Reflection
{
    /// <summary>
    /// Holds metadata of entity managed by AbMapper. Uses reflection to collection information.
    /// </summary>
    public class Entity
    {
        /// <summary>
        /// Type of the managed entity.
        /// </summary>
        public Type Type { get; internal set; }
        
        /// <summary>
        /// Table name to be used by datasources to persist. Defaults to type name if not set explicitly.
        /// </summary>
        public string TableName { get; internal set; }

        private readonly List<Property> _properties = new List<Property>();
        
        /// <summary>
        /// Enumerable if all properties (with metadata) this entity has.
        /// </summary>
        public IList<Property> Properties => _properties.AsReadOnly();
        
        /// <summary>
        /// Enumerable of all primary key properties this entity has.
        /// </summary>
        public IList<Property> PrimaryKeys => _properties.FindAll(p => p.IsPrimary).AsReadOnly();

        /// <summary>
        /// Enumerable of all foreign key properties this entity has.
        /// </summary>
        public IList<Property> ForeignKeys => _properties.FindAll(p => p.IsForeign).AsReadOnly();
        
        /// <summary>
        /// True if a primary key was discovered while gathering information with reflection.
        /// </summary>
        public bool HasPrimaryKey { get; internal set; }

        /// <summary>
        /// True if this entity has relationships to other entites.
        /// </summary>
        public bool HasRelatedEntity { get; set; }
        
        /// <summary>
        /// Indicates that this entity is managed by the mapper and was not exposed by the user.
        /// </summary>
        public bool IsMapperManaged { get; internal set; }
        
        private readonly List<ForeignEntityInfo> _foreignEntityInfos = new List<ForeignEntityInfo>();

        /// <summary>
        /// Stores information about relationships to other entities.
        /// </summary>
        public IEnumerable<ForeignEntityInfo> ForeignEntityInfos => _foreignEntityInfos.AsReadOnly();

        /// <summary>
        /// Gathers metadata of the given entity and saves it to the public properties of the Entity class.
        /// </summary>
        /// <param name="type">Type to analyze</param>
        /// <exception cref="AbCollectionPrimaryKeyCountException">Thrown if no primary key was found</exception>
        public Entity(Type type)
        {
            Type = type;
            TableName = type.Name.ToLower();
            // Read property information
            foreach (var propertyInfo in type.GetProperties())
            {
                if (propertyInfo.GetMethod == null || propertyInfo.SetMethod == null) continue; // Require getter and setter
                
                var prop = new Property { PropertyInfo = propertyInfo };
                string colName = null;
                
                if (propertyInfo.GetCustomAttribute(typeof(PrimaryKeyAttribute)) != null)
                {
                    HasPrimaryKey = true;
                    prop.IsPrimary = true;
                }
                
                if (propertyInfo.GetCustomAttribute(typeof(NotNullAttribute)) != null) prop.NotNull = true;
                if (propertyInfo.GetCustomAttribute(typeof(SerialAttribute)) != null) prop.IsSerial = true;
                if (propertyInfo.GetCustomAttribute(typeof(ExcludedAttribute)) != null) prop.IsExcluded = true;
                
                var colNameAttr = (ColumnNameAttribute) propertyInfo.GetCustomAttribute(typeof(ColumnNameAttribute));
                if (colNameAttr != null) colName = colNameAttr.Name.ToLower();
                
                var delBehaviourAttr = (DeleteBehaviourAttribute) propertyInfo.GetCustomAttribute(typeof(DeleteBehaviourAttribute));
                if (delBehaviourAttr != null) prop.DeleteBehaviour = delBehaviourAttr.DeleteBehaviour;

                prop.Name = propertyInfo.Name;
                prop.ColumnName = colName ?? prop.Name.ToLower();
                prop.Type = propertyInfo.PropertyType;
                prop.IsEnum = propertyInfo.PropertyType.IsEnum;
                _properties.Add(prop);
            }
            
            if (!HasPrimaryKey) throw new AbCollectionPrimaryKeyCountException($"Found no primary key on entity {type.Name}. Expected at least one primary key.");
        }

        /// <summary>
        /// Constructor for mapper managed entities.
        /// </summary>
        internal Entity()
        {
            
        }

        /// <summary>
        /// Adds information about a related entity.
        /// </summary>
        /// <param name="info"></param>
        public void AddRelatedEntity(ForeignEntityInfo info)
        {
            _foreignEntityInfos.Add(info);
            
            if (info.ForeignEntity != this)
                _properties.AddRange(info.ForeignKeyInfos.Select(fkInfo => fkInfo.LocalProperty));
        }

        /// <summary>
        /// Removes the given property from properties list.
        /// </summary>
        /// <param name="property"></param>
        public void RemoveProperty(Property property)
        {
            _properties.Remove(property);
        }

        /// <summary>
        /// Adds the given property to properties list.
        /// </summary>
        /// <param name="property"></param>
        public void AddProperty(Property property)
        {
            _properties.Add(property);
        }
    }
}